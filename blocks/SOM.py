#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb  3 10:05:27 2021

@author: aravilievich
"""

import torch
import numpy as np
import itertools
import tqdm
import matplotlib.pyplot as plt
import cv2
import os
from sklearn.manifold import MDS, Isomap, TSNE
import pickle
#%%

class SOM:
    """
    Common class to code Self-oranising Map of Kohonen
    There are list of some saved variables:
    self.som_type - the type of SOM, "KSOM" means standart Kohonen's map
    self.epsilon, self.eta - parameters of SOM learning, starting step and it's evolution
    self.device - device for computation, better to change in creation,
    but common funtional must work even if changed during work
    self.map_size - shape of the SOM
    self.vector_len - SOM's vector len
    self.som_map - the map in torch tensor format
    self.idx_map - the map of indeces, for fast distance calculation
    self.dists2 - avalible after call of find_dist function (or other with call itself), 
    it keeps the distances from last given x to all neurons of the map
    self.lbl_probs and self.lbl_idx, avalible after call of labeling function,
    they keep probabilities and ideces for labels of labeled neurons
    self.labels_list and self.labels_map also avalible after labeling,
    they are bidirectional pointers from given labels to their's indeces in map and backward;
    
    """
    def __init__(self, map_size = (10,10), vector_len = 10, use_gpu = True, seed = None,
                 epsilon = 1, eta = 1, from_file =  False, som_type = 'KSOM',
                 file_name = "/../weights/som/ksom_weights_image_36", file_format = "1d_text"):
        self.som_type = som_type
        self.epsilon = epsilon 
        self.eta = eta
        if seed is not None:
            torch.manual_seed(seed)
        if use_gpu:
            self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        else:
            self.device = torch.device("cpu")
        
        if from_file:
            self.import_trained_weights(name = os.path.dirname(__file__) + file_name, form = file_format)
        else:
            self.map_size = map_size
            self.vector_len = vector_len        
            self.som_map = torch.rand(map_size[0], map_size[1], vector_len, device = self.device)
        # The map of ideces to fast distancies computing of updating step
        self.idx_map = torch.tensor(list(itertools.product(range(map_size[0]), range(map_size[1])))).reshape(map_size[0],map_size[1],2)
        self.idx_map = self.idx_map.to(self.device)
        
    def import_trained_weights(self, name="/../weights/som/ksom_weights_image_36", form = "1d_text"):
        file = open(name, "r")
        wgt_list = [[float(num) for num in line.split(" ")] for line in file.read().split("\n")]
        
        if form == "1d_text":
            self.vector_len = len(wgt_list[0])
            map_len = np.sqrt(len(wgt_list)).astype(int)
            
            self.map_size = (map_len, map_len)
            self.som_map = torch.tensor(wgt_list).reshape((map_len,map_len,self.vector_len)).to(self.device)
        else:
            print("Unknown format")
        file.close()
        
    def reset_device(self, device = "cuda:0"):
        self.device = torch.device(device)
        self.som_map = self.som_map.to(self.device)
        self.idx_map = self.idx_map.to(self.device)
        if 'self.dists2' in self.__dict__:
            self.dists2 = self.dists2.to(self.device)
        if 'lbl_probs' in self.__dict__:
            self.lbl_probs = self.lbl_probs.to(self.device)
            self.lbl_idx = self.lbl_idx.to(self.device)
        
    def get_map(self, as_np = False):
        if as_np:
            return self.som_map.cpu().detach().numpy()
        return self.som_map 
    
    def put_x_to_tensor(self, x):
        if type(x) != torch.Tensor:
            x = torch.tensor(x, dtype = torch.float32).to(self.device)
        x = x.to(self.device)
        return x
    
    # this method count euclidean dists to all neurons, saving the map pof dists
    def find_dists2(self, x):
        x = self.put_x_to_tensor(x)
        self.dists2 = torch.sum(torch.pow(self.som_map - x, 2), (2))
        return self.dists2
    
    # this method calls the upper method and find bmu, saving the map pof dists
    def find_bmu(self, x):
        x = self.put_x_to_tensor(x)
        self.dists2 = torch.sum(torch.pow(self.som_map - x, 2), (2))
        self.bmu_idx = torch.argmin(self.dists2).cpu().detach().numpy()
        return self.bmu_idx // self.map_size[0], self.bmu_idx % self.map_size[0]
    
    # train algorithm on one x vector
    # parameters epsilon and eta may be redefined in arguments (by define they are savecd in class)
    def train_one_x(self, x, **arguments):
        x = self.put_x_to_tensor(x)
        epsilon = self.epsilon
        eta = self.eta
        if "epsilon" in arguments:
            epsilon = arguments['epsilon']
        if 'eta' in arguments:
            eta = arguments['eta']
        bmu = torch.tensor(self.find_bmu(x)).to(self.device)
        if self.som_type == "KSOM":
            phys_dist = torch.sum(torch.pow(self.idx_map - bmu, 2), (2)).type(torch.float32)
            neib_func = torch.exp(-phys_dist/(2*eta**2))
            #update
            self.som_map = self.som_map + epsilon*torch.einsum('ab,abc->abc', neib_func,(x - self.som_map))
        elif self.som_type == "DSOM":
            phys_dist = torch.sum(torch.pow(self.idx_map - bmu, 2), (2)).type(torch.float32)
            d = torch.pow((x - self.som_map[bmu[0], bmu[1]]), 2).sum()
            neib_func = torch.exp(-phys_dist/d/(eta**2))
            #update
            self.som_map = self.som_map + epsilon*torch.einsum('ab,abc->abc', 
                                                               neib_func, 
                                                               torch.pow(x - self.som_map, 2)*torch.sign(x - self.som_map)) 
        else:
            print("No such SoM type:", self.som_type)
        
    def train_one_epoch(self, X, **arguments):
        if ('verbose' in arguments) and (arguments['verbose'] > 3):
            for x in tqdm.tqdm(X):
                self.train_one_x(x, **arguments)
        else:
            for x in X:
                self.train_one_x(x, **arguments)
            
    # params_evolution = 'auto' ask function to use predefined algorithme for epsilon and eta changing
    # verbose <= 1 - no output information, verbose > 1 only nubmer of epochs, 
    # verbose > 2 tqdm timebars, verbose > 5 images with weights will be draw
    def train(self, X, n_epochs = 10, verbose = 1, params_evolution = "auto", device = 'auto', **arguments):
        
        if  device != 'auto':
            self.reset_device(device)
        eps_i = 1.0
        eps_f = 0.01
        eta_i = 5.0
        eta_f = 0.01
        eta = eta_i
        eps = eps_i
        evol_each_obj = False
        
        if "epsilon" in arguments:
            eps = arguments['epsilon']
        if 'eta' in arguments:
            eta = arguments['eta']
        if "epsilon_i" in arguments:
            eps_i = arguments['epsilon_i']
        if 'eta_i' in arguments:
            eta_i = arguments['eta_i']
        if "epsilon_f" in arguments:
            eps_f = arguments['epsilon_f']
        if 'eta_f' in arguments:
            eta_f = arguments['eta_f']
        if 'evol_each_obj' in arguments:
            evol_each_obj =  arguments['evol_each_obj']
            
        for ep in range(n_epochs):
            if verbose > 1:
                print("\n Start of epoch", ep + 1)
            if params_evolution == "auto":
                eta = eta_i*(eta_f/eta_i)**(ep/(n_epochs - 1 + 1e-10))
                eps = eps_i*(eps_f/eps_i)**(ep/(n_epochs - 1 + 1e-10))
            if verbose > 1:
                print(eta,eps,"\n")
            if (not evol_each_obj) or params_evolution != "auto":
                self.train_one_epoch(X, epsilon = eps, eta = eta, verbose = verbose)
            else:
                if verbose > 2:
                    for i, x in tqdm.tqdm(enumerate(X)):
                        st = (ep + i /len(X)) /(n_epochs - 1 + 1e-10)
                        eta = eta_i*(eta_f/eta_i)**(st)
                        eps = eps_i*(eps_f/eps_i)**(st)
                        self.train_one_x(x, epsilon = eps, eta = eta, verbose = verbose)
                else:
                    for i, x in enumerate(X):
                        st = (ep + i /len(X)) /(n_epochs - 1 + 1e-10)
                        eta = eta_i*(eta_f/eta_i)**(st)
                        eps = eps_i*(eps_f/eps_i)**(st)
                        self.train_one_x(x, epsilon = eps, eta = eta, verbose = verbose)
            if verbose > 5:
                b_image = self.create_image()
                plt.imshow(b_image)
                plt.show()
                
    # assign labels for cells according to given labels
    # the gaussian repose around bmu is used as method
    def labeling(self, X, labels, sigma = 1.0, list_cls = None):
        # creating bidirectional map for labels, so they can be of any instance
        if list_cls is None:
            self.labels_list  = np.sort(np.unique(labels))
        else:
            self.labels_list = np.sort(list_cls)
        self.labels_dict = {}
        for i, label in enumerate(self.labels_list):
            self.labels_dict[label] = i      
                
        self.sigma = sigma
        self.accumulator = torch.zeros(self.map_size[0], self.map_size[1], 
                                  len(self.labels_list), device = self.device)
        lbl_counter = torch.zeros(len(self.labels_list), device = self.device) + 1e-10
        
        for i, x in enumerate(X):
            lbl_idx = self.labels_dict[labels[i]]
            lbl_counter[lbl_idx] += 1
                        
            bmu = self.find_bmu(x)
            gaus_dists = torch.exp(-torch.sqrt(self.dists2) / self.sigma)
            
            self.accumulator[:,:,lbl_idx] += gaus_dists/gaus_dists[bmu[0], bmu[1]]
        
        self.accumulator[:,:] = self.accumulator[:,:] / lbl_counter
        self.lbl_probs = self.accumulator
        _, self.lbl_idx = torch.max(self.accumulator, 2)
        labeling = np.array([[self.labels_list[self.lbl_idx[i,j]] 
                     for j in range(self.map_size[1])]
                     for i in range(self.map_size[0])])
        return labeling
            
    def put_labels(self, labels, list_cls = None): 
        
        if list_cls is None:
            self.labels_list  = np.sort(np.unique(labels))
        else:
            self.labels_list = np.sort(list_cls)
        self.labels_dict = {}
        for i, label in enumerate(self.labels_list):
            self.labels_dict[label] = i 
            
            
    # only for graphical image
    def create_image(self):
        map_size = self.map_size
        image_side = int(np.sqrt(self.vector_len))
        big_image = np.zeros((image_side*map_size[0],image_side*map_size[1]))
        weights = self.get_map(True)
        for i in range(map_size[0]):
            for j in range(map_size[1]):
                w = weights[i,j]
                big_image[i*image_side:(i+1)*image_side,
                          j*image_side:(j+1)*image_side] = w.reshape((image_side,image_side))
        return big_image
    
    def make_2d_projection(self, mode = "isomap", use_pretrained = True, ds = 'mnist'):
        reshaped_map = self.som_map.reshape((self.map_size[0]*self.map_size[1], self.vector_len))
        folder = "/home/aravilievich/Documents/Artem_M/My code/mmsom/weights/som/2d_projectors/"
        if mode == "svd":
            reshaped_map = torch.einsum('ab->ba', reshaped_map)
            U, S, V = torch.svd(reshaped_map)
            projector = U[:,:2]
            res = torch.einsum('abc,cd->abd', self.som_map, projector).cpu().numpy()
        if mode == "mds":
            emb = MDS(n_components=2)
            res = emb.fit_transform(reshaped_map.cpu().numpy().astype(np.float64))
            res = res.reshape((self.map_size[0],self.map_size[1],res.shape[1]))
        if mode == "isomap":
            if use_pretrained:
                emb = pickle.load( open( folder + ds + "_isomap.pickle", "rb" ) )
                res = emb.transform(reshaped_map.cpu().numpy().astype(np.float64))
            else:
                emb = Isomap(n_components=2)
                res = emb.fit_transform(reshaped_map.cpu().numpy())
            res = res.reshape((self.map_size[0],self.map_size[1],res.shape[1]))
        if mode == "tsne":
            emb = TSNE(n_components=2)
            res = emb.fit_transform(reshaped_map.cpu().numpy())
            res = res.reshape((self.map_size[0],self.map_size[1],res.shape[1]))
        return res
    
    def create_2d_proj_image(self, mode = "isomap", use_pretrained = True,
                             ds = 'mnist'):
        proj_2d = self.make_2d_projection(mode, use_pretrained, ds = ds)
        try:
            lbls = self.lbl_idx.cpu().numpy()
        except:
            lbls = np.zeros(self.map_size)
        for i in range(10):
            plt.plot(proj_2d[i,:,0], proj_2d[i,:,1], color = "black",
                     linewidth = 0.3, zorder= -1)
        for i in range(10):
            plt.plot(proj_2d[:,i,0], proj_2d[:,i,1], color = "black",
                     linewidth = 0.3, zorder= -1)
        cmap =  plt.cm.get_cmap('tab20', np.max(lbls) + 1)
        sc = plt.scatter(proj_2d[:,:,0].ravel(), proj_2d[:,:,1].ravel(), s=100,
                c =  lbls.ravel(), zorder= 1, cmap = cmap)
        cb = plt.colorbar(sc)
        fn_tmp_image = os.path.dirname(__file__) + "/../tmp/2d_tmp_image.png"
        plt.savefig(fn_tmp_image)
        cb.remove()
        plt.clf()
        return cv2.cvtColor(cv2.imread(fn_tmp_image), cv2.COLOR_BGR2RGB)
    
    def test(self, X, Y):
        
        f_num = len(self.labels_list)
        conf_matrix = np.zeros((f_num, f_num))
        count_obj = np.zeros((f_num))
        som_err = 0
        self.bmus_counter = np.zeros(self.lbl_idx.shape)
        
        for i,(x,y) in enumerate(zip(X,Y)):
            bmu = self.find_bmu(x)
            som_err = som_err + np.sqrt(self.dists2.cpu().detach().numpy())
            pred_lbl_idx = self.lbl_idx[bmu[0],bmu[1]]
            self.bmus_counter[bmu[0],bmu[1]] = self.bmus_counter[bmu[0],bmu[1]] + 1
            conf_matrix[self.labels_dict[y], pred_lbl_idx] +=1
            count_obj[self.labels_dict[y]] += 1
        
        self.metrics = []
        # add accuracy
        self.metrics.append(np.trace(conf_matrix) / len(Y))
        # arr som error
        self.metrics.append(som_err)
        self.conf_matrix = conf_matrix / count_obj * 100
        return self.metrics, self.conf_matrix 
    
    def predict(self, X):
        preds = []
        for i,x in enumerate(X):
            bmu = self.find_bmu(x)
            pred_lbl_idx = self.lbl_idx[bmu[0],bmu[1]]
            preds.append(self.labels_list[pred_lbl_idx])
        return preds
    
    def predict_proba(self, X):
        probs = torch.tensor(len(X), self.lbl_probs.shape[2]).to(self.device)
        for i,x in enumerate(X):
            bmu = self.find_bmu(x)
            probs[i] = self.lbl_probs[bmu[0],bmu[1]]
        return probs
        

#%%
# class tests 
if __name__ == "__main__":
    # load data for test
    db_folder = os.path.dirname(__file__) + "/../../Databases/New_MNIST_SpokenDigits_databse"
    n_train = 60000
    n_test = 10000
    im_x_train_all = np.load(db_folder + "/data_wr_train.npy", allow_pickle=True)
    sp_x_train_all = np.load(db_folder + "/data_sp_train.npy", allow_pickle=True)
    index_train_all = np.load(db_folder + "/labels_train.npy", allow_pickle=True)
    im_x_test_all = np.load(db_folder + "/data_wr_test.npy", allow_pickle=True)
    sp_x_test_all = np.load(db_folder + "/data_sp_test.npy", allow_pickle=True)
    index_test_all = np.load(db_folder + "/labels_test.npy", allow_pickle=True)
    
    im_x_train = np.copy(im_x_train_all[:n_train])
    sp_x_train = np.copy(sp_x_train_all[:n_train])
    index_train = np.copy(index_train_all[:n_train])
    im_x_label = np.copy(im_x_train_all[55000:55000+n_train,:])
    sp_x_label = np.copy(sp_x_train_all[55000:55000+n_train,:])
    index_label = np.copy(index_train_all[55000:55000+n_train])
    im_x_test = np.copy(im_x_test_all[:n_test])
    sp_x_test = np.copy(sp_x_test_all[:n_test])
    index_test = np.copy(index_test_all[:n_test])
#%%
#Small tests, delete all below after finishinng the code
    som = SOM(map_size = (10, 10), vector_len = 784, seed = 777, use_gpu = True)
#%% 
    som.train(im_x_train, verbose = 3, n_epochs = 8, device = "cpu")
#%% 
    lbls = som.labeling(im_x_label, index_label, sigma = 1.0)
    print(lbls)
#%% 
    acc, c_m = som.test(im_x_test, index_test)
    print(acc)
    plt.imshow(c_m)
#%%
    big_image = som.create_image()
    plt.imshow(big_image)
    plt.show()

#%% 
    som = SOM(from_file=True, use_gpu = True)
    lbls = som.labeling(im_x_label, index_label, sigma = 1.0)
    acc, c_m = som.test(im_x_test, index_test)
    
    big_image = som.create_image()
    plt.imshow(big_image)
    plt.show()
    #%%
    img = som.create_2d_proj_image('isomap')
    #%%
    plt.imshow(img)
    #%%
    
    ########### 
    # Test on spoken MNIST
    #########
    # ready weights
    som = SOM(from_file = True, file_name = "/../weights/som/ksom_weights_speech_16")
    
    #%%
    lbls = som.labeling(sp_x_label, index_label, sigma = 0.1)
    acc, c_m = som.test(sp_x_test, index_test)
    
    print(acc)
    plt.imshow(c_m)    
    #%%        
    # train weights
    som = SOM(map_size = (16, 16), vector_len = 507, seed = 777, use_gpu = True)
    som.train(sp_x_train, verbose = 3, n_epochs = 10, eta_i = 10.0)
    #%%
    lbls = som.labeling(sp_x_label, index_label, sigma = 0.1)
    print(lbls)
#%% 
    acc, c_m = som.test(sp_x_test, index_test)
    print(acc)
    plt.imshow(c_m)
    
    #%%
    #####
    #TEST of DSOM
    ######
    som = SOM(map_size = (10, 10), vector_len = 784, seed = 777, use_gpu = True, som_type='DSOM')
    #%% 
    som.train(im_x_train, verbose = 3, n_epochs = 10, params_evolution = "manual", 
              epsilon = 0.005, eta = 0.1)
    #%% 
    lbls = som.labeling(im_x_label, index_label, sigma = 0.1)
    print(lbls)
    #%% 
    acc, c_m = som.test(im_x_test, index_test)
    print(acc)
    plt.imshow(c_m)
    #%%
    big_image = som.create_image()
    plt.imshow(big_image)
    plt.show()