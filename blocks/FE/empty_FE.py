#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  8 18:09:32 2021

@author: aravilievich
"""

import sys
import os

sys.path.append(os.path.dirname(__file__))
from abstract_FE import Abstract_FE

import torch
import numpy as np

class Empty_FE(Abstract_FE):
    
    def __init__(self, use_gpu = True, **kwargs):
        super().__init__(**kwargs)
        self.fe_type = "None"
        self.train_on_all_only = False
        if use_gpu:
            self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        else:
            self.device = torch.device("cpu")
    
    def get_feature(self, x, **kwargs):
        x = self.put_x_to_tensor(x)
        return x

    def get_features(self, X, **kwargs):
        X = self.put_x_to_tensor(X)
        return X
    
    def put_x_to_tensor(self, x):
        if type(x) != torch.Tensor:
            x = torch.tensor(x, dtype = torch.float32).to(self.device)
        x = x.to(self.device)
        return x
    
    def train(self, X, Y = None, **kwargs):
        pass
    
    def train_one_x(self, x, y = None, **kwargs):
        pass
    
    def train_on_classes(self, X, Y = None, **kwargs):
        pass
    
    def train_on_errors(self, X, Y = None, **kwargs):
        pass