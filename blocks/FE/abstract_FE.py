#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  8 18:09:32 2021

@author: aravilievich
"""

from abc import ABC, abstractmethod
import pickle
import sys
import os

FILES_FOLDER = os.path.dirname(__file__) + "/../../weights/fe/"

class Abstract_FE(ABC):
    
    def __init__(self, **kwargs):
        if "load" in kwargs:
            if kwargs["load"]:
                loaded = pickle.load( open(FILES_FOLDER + kwargs["filename"] + ".pickle", "rb" ) )
                self.__dict__.update(loaded.__dict__)

    @abstractmethod
    def get_features(self, X, **kwargs):
        pass
    
    @abstractmethod
    def train_on_errors(self, X, Y = None, **kwargs):
        pass
    
    @abstractmethod
    def train_on_classes(self, X, Y = None, **kwargs):
        pass
    
    def save(self, filename = "tmp_FE"):
        pickle.dump(self, file = open(FILES_FOLDER + filename + ".pickle", "wb" ) )
        return