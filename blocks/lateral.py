#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb  3 10:05:27 2021

@author: aravilievich
"""

import torch
import numpy as np
import itertools
import tqdm
import matplotlib.pyplot as plt
import cv2
import os
from sklearn.manifold import MDS, Isomap, TSNE
import pickle
#%%

class Lateral:
    """
    Common class for lateral connections
    
    """
    def __init__(self,  size = (10,10), use_gpu = True, 
                 random_weights = True, seed = None, force = 1):

        if seed is not None:
            torch.manual_seed(seed)
        if use_gpu:
            self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        else:
            self.device = torch.device("cpu")
        
        if random_weights:
            self.connections = torch.rand(size, device=self.device)*force
        else:
            self.connections = torch.zeros(size, device=self.device)
        
        
    def reset_device(self, device = "cuda:0"):
        self.device = torch.device(device)
        ...

    def put_x_to_tensor(self, x):
        if type(x) != torch.Tensor:
            x = torch.tensor(x, dtype = torch.float32).to(self.device)
        x = x.to(self.device)
        return x
    

    def train_one_x(self, point, alpha = 1e-5, **arguments):
        self.connections[point] += alpha
        pass
    
    def inhibition(self, r = 0.5, **arguments):
        self.connections = self.connections*r
                

#%%
# class tests 
if __name__ == "__main__":
    # load data for test
    db_folder = os.path.dirname(__file__) + "/../../Databases/New_MNIST_SpokenDigits_databse"
    n_train = 60000
    n_test = 10000
    im_x_train_all = np.load(db_folder + "/data_wr_train.npy", allow_pickle=True)
    sp_x_train_all = np.load(db_folder + "/data_sp_train.npy", allow_pickle=True)
    index_train_all = np.load(db_folder + "/labels_train.npy", allow_pickle=True)
    im_x_test_all = np.load(db_folder + "/data_wr_test.npy", allow_pickle=True)
    sp_x_test_all = np.load(db_folder + "/data_sp_test.npy", allow_pickle=True)
    index_test_all = np.load(db_folder + "/labels_test.npy", allow_pickle=True)
    
    im_x_train = np.copy(im_x_train_all[:n_train])
    sp_x_train = np.copy(sp_x_train_all[:n_train])
    index_train = np.copy(index_train_all[:n_train])
    im_x_label = np.copy(im_x_train_all[55000:55000+n_train,:])
    sp_x_label = np.copy(sp_x_train_all[55000:55000+n_train,:])
    index_label = np.copy(index_train_all[55000:55000+n_train])
    im_x_test = np.copy(im_x_test_all[:n_test])
    sp_x_test = np.copy(sp_x_test_all[:n_test])
    index_test = np.copy(index_test_all[:n_test])
#%%
#Small tests, delete all below after finishinng the code
    som = SOM(map_size = (10, 10), vector_len = 784, seed = 777, use_gpu = True)
#%% 
    som.train(im_x_train, verbose = 3, n_epochs = 8, device = "cpu")
#%% 
    lbls = som.labeling(im_x_label, index_label, sigma = 1.0)
    print(lbls)
#%% 
    acc, c_m = som.test(im_x_test, index_test)
    print(acc)
    plt.imshow(c_m)
#%%