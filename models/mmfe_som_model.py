#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: aravilievich
"""
import sys
import os
import torch
import torch.fft

sys.path.append(os.path.dirname(__file__))
from abstract_model import AbstractModel
from fe_som_model import FE_SOM_Model

sys.path.append(os.path.dirname(__file__) + "/../blocks")
from SOM import SOM
from lateral import Lateral

sys.path.append(os.path.dirname(__file__) + "/../blocks/FE")
from abstract_FE import Abstract_FE
from empty_FE import Empty_FE

import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm
import math
#%%

class MMFE_SOM_Model(AbstractModel):
    
    def __init__(self, take_trained_fesoms = True, fesoms = (None, None),
                 fesoms_descr = (None, None), use_gpu = True, 
                 random_lat_weights = False, seed = None, 
                 labels_list = None, **kwargs):     
        # class continer of full multimodel framework
        # it's creation done based on fe_som_model or on its description to create
        # fesoms must contain ready objects
        # fesons_descr - dict with descibed parameters (defo object will be created elseway)
        # ex. som_descr = {vector_len : 50, seed : 0, use_gpu : True, FE_type : "backwardCNN", loadFE : False}
        
        if "load" in kwargs:
            if kwargs["load"]:
                super().__init__(**kwargs)
                return None  
            
        self.use_gpu = use_gpu
        self.random_lat_weights = random_lat_weights
        self.seed = seed
        self.labels_list = labels_list
            
        if take_trained_fesoms:
            self.fesoms = fesoms
        else:
            self.fesoms = []
            for som_descr in fesoms_descr:
                self.fesoms.append(FE_SOM_Model(som_descr))
        
        self.act_counters = []
        self.act_counters_par_class = []
        for i in range(len(self.fesoms)):
            self.act_counters.append(np.zeros((self.fesoms[i].som.map_size)))
            self.act_counters_par_class.append(np.zeros(10))
        
        if use_gpu:
            self.device = 'cuda'
        else:
            self.device = 'cpu'
                
        self.resom_lat_dict = None
        self.cdz_lat_list = None
        
    def init_resom_lateral(self):
        self.resom_lat_dict = dict()
        for i in range(len(self.fesoms)):
            for j in range(i + 1, len(self.fesoms)):
                som1size = self.fesoms[i].map_size
                som2size = self.fesoms[j].map_size
                self.resom_lat_dict[(i, j)] = Lateral(size = som1size + som2size,
                                                      use_gpu = self.use_gpu,
                                                      random_weights = self.random_lat_weights,
                                                      seed = self.seed)
                
    def init_cdz_lateral(self, shape = (15,15), random_lat_weights = True, force = 1):
        self.cdz_lat_list = []
        self.possible_indexes = 'hifklmnopqrst'
        for i in range(len(self.fesoms)):
            som1size = self.fesoms[i].map_size
            self.cdz_lat_list.append(Lateral(size = som1size + shape,
                                             use_gpu = self.use_gpu,
                                             random_weights = random_lat_weights ,
                                             seed = self.seed,
                                             force = force))
            
    def create_win_filter(self, cdz_collector_shape):                                
        window = np.array([[0, 0, 1, 0, 0],
                            [0, 1, 3, 1, 0],
                            [1, 3, 5, 3, 1],
                            [0, 1, 3, 1, 0],
                            [0, 0, 1, 0, 0]]) / 25
        big_filter = np.zeros(cdz_collector_shape)
        big_filter[:window.shape[0], :window.shape[1]] = window
        big_filter = np.roll(big_filter , -2, axis = 0)
        big_filter = np.roll(big_filter , -2, axis = 1)
        final_window = torch.tensor(big_filter, 
                     dtype = torch.float,
                     device = self.device)
        self.flt = torch.fft.fftn(final_window, s = cdz_collector_shape)   
        
            
    def train(self, X, Y = None, train_lateral = True, train_lat_mod = 'resom',
              resom_train_params = (1.,1.,), cdz_train_params = (1., 0.01, 0.9, 5, 0.1, 0.3),
              X_lbls = None, Y_lbls = None,
              list_cls = None, labeling = True, X_test = None, Y_test = None,
              train_soms = False, train_soms_params = (None, None),
              num_epochs = 1, verbose = 2, test_each = None,
              save_activations = False, ignore_activations = False,
              cdz_shape = (15,15), smooth_cdz = True,
              random_cdz_init = True, random_cdz_init_coef = 1, 
              normalise_lat_cdz = False, with_inhibition = False,  
              cut_inhibition = True, save_resom_num_evolution = False, **kwargs):
        # if you wish to train the soms,
        # train som params must be defined for each som and tuple of tuples
        # format for one som: (eps_i, eps_f, eta_i, eta_f, sigma)
        self.tests = []
        if test_each is None:
            test_each = len(X[0])
            
        self.train_lat_mod = train_lat_mod
        self.train_soms_params = train_soms_params
        
        if save_resom_num_evolution:
            self.resom_num_evolution = dict()
            for i in range(len(self.fesoms)):
                for j in range(i + 1, len(self.fesoms)):
                    self.resom_num_evolution[(i, j)] = [0]
            
        #####################
        ###TRAINING##########
        ####################
        for ep in range(num_epochs):
            self.past_activations = []
            itr = range(len(X[0]))
            if verbose > 2:
                itr = tqdm(range(len(X[0])))
            for i in itr:
                if train_soms:
                    for j in range(self.fesoms):
                        fesom = self.fesoms[j]
                        x_fe = fesom.FE.get_features(X[j][i:i+1], **kwargs)
                        st = (ep + i /len(X[0])) / (num_epochs + 1e-100)
                        eps_i = train_soms_params[j][0]
                        eps_f = train_soms_params[j][1]
                        eta_i = train_soms_params[j][2]
                        eta_f = train_soms_params[j][3]
                        eta = eta_i*(eta_f/eta_i)**(st)
                        eps = eps_i*(eps_f/eps_i)**(st)
                        fesom.som.train_one_epoch(x_fe, epsilon = eps, eta = eta, verbose = verbose) 
                if train_lateral:
                    if 'resom' in train_lat_mod:
                        if self.resom_lat_dict is None:
                            self.init_resom_lateral()
                        for k1 in range(len(self.fesoms)):
                            for k2 in range(k1 + 1, len(self.fesoms)):
                                lat = self.resom_lat_dict[(k1, k2)]
                                
                                x_fe = self.fesoms[k1].FE.get_features(X[k1][i:i+1], **kwargs)
                                bmu1 = self.fesoms[k1].som.find_bmu(x_fe[0])
                                sigma = train_soms_params[k1][4]
                                act1 = torch.exp(-torch.min(torch.sqrt(self.fesoms[k1].som.dists2))/sigma)
                                self.act_counters[k1][bmu1[0], bmu1[1]] = self.act_counters[k1][bmu1[0], bmu1[1]] + 1
                                self.act_counters_par_class[k1][Y[i]] += act1
                                
                                x_fe = self.fesoms[k2].FE.get_features(X[k2][i:i+1], **kwargs)
                                bmu2 = self.fesoms[k2].som.find_bmu(x_fe[0])
                                sigma = train_soms_params[k2][4]
                                act2 = torch.exp(-torch.min(torch.sqrt(self.fesoms[k2].som.dists2))/sigma)
                                self.act_counters[k2][bmu2[0], bmu2[1]] = self.act_counters[k2][bmu2[0], bmu2[1]] + 1
                                self.act_counters_par_class[k2][Y[i]] += act2
                                
                                if save_activations:
                                    self.past_activations.append((act1, act2))
                                
                                point = (bmu1[0], bmu1[1], bmu2[0], bmu2[1])
                                alpha = resom_train_params[0]*act1*act2
                                if ignore_activations:
                                    alpha = resom_train_params[0]
                                if save_resom_num_evolution and lat.connections[point] == 0:
                                    self.resom_num_evolution[(k1, k2)].append(i)
                                lat.train_one_x(point, alpha = alpha)
                                
                                
                    if 'cdz' in train_lat_mod:
                        self.flt = None
                        if self.cdz_lat_list is None:
                            self.init_cdz_lateral(shape=cdz_shape, 
                                                  random_lat_weights = random_cdz_init,
                                                  force = random_cdz_init_coef)
                            for k in range(len(self.fesoms)):
                               s = torch.sum(self.cdz_lat_list[k].connections)
                               if s != 0:
                                   self.cdz_lat_list[k].connections = self.cdz_lat_list[k].connections/s
                        self.possible_indexes = 'hifklmnopqrst'
                        cdz_act_list = []
                        cdz_max_acts = []
                        cdz_collector = torch.zeros(cdz_shape, device=self.device)
                        for k in range(len(self.fesoms)):
                            x_fe = self.fesoms[k].FE.get_features(X[k][i:i+1], **kwargs)
                            self.fesoms[k].som.find_bmu(x_fe[0])
                            sigma = train_soms_params[k][4]
                            act = torch.exp(-torch.sqrt(self.fesoms[k].som.dists2)/sigma)
                            cdz_act_list.append(act)
                            cdz_max_acts.append(torch.max(act))
                            
                        for k in range(len(self.fesoms)):
                            #summ of activations among different maps
                            cdz_collector[:] = 1
                            for m in range(len(self.fesoms)):
                                #if k==m:
                                #   continue
                                needed_inx = self.possible_indexes[:len(cdz_shape)]
                                lat_act = torch.einsum('ab,ab' + needed_inx + '->' + needed_inx,
                                                       cdz_act_list[m],
                                                       self.cdz_lat_list[m].connections)
                                cdz_collector[:] = cdz_collector[:]*lat_act
                            #plt.imshow(cdz_collector)
                            #plt.show()
                            
                            #Filter for add homogenity between neighbours (may be other ideas)
                            if smooth_cdz:
                                fftn = torch.fft.fftn(cdz_collector)
                                # coded only for 2-dim
                                if self.flt is None:
                                    self.create_win_filter(cdz_collector.shape)
                                
                                new_fftn = fftn*self.flt
    
                                cdz_collector = torch.fft.ifftn(new_fftn).real
                                
                                
                            self.cdz_train_q = cdz_train_params[2]
                            self.cdz_train_sig_froce = cdz_train_params[3]
                            self.noise_force = cdz_train_params[4]
                            if with_inhibition:
                                self.inhib_lvl = cdz_train_params[5]
                            cdz_selector = ((cdz_collector - torch.quantile(cdz_collector, self.cdz_train_q)) /
                                torch.std(cdz_collector))
                            cdz_selector = torch.sigmoid(cdz_selector*self.cdz_train_sig_froce) 
                            if with_inhibition:
                                cdz_selector = cdz_selector - self.inhib_lvl
                            
                            #plt.imshow(cdz_selector)
                            #plt.show()
                            
                            act = cdz_act_list[k] 
                            needed_inx = self.possible_indexes[:len(cdz_shape)]
                            lat_step = torch.einsum('ab,' + needed_inx + '->ab' + needed_inx,
                                                   act, cdz_collector*cdz_selector)
                            
                            old_sum = torch.sum(self.cdz_lat_list[k].connections)
                            st = (ep + i /len(X[0])) /(num_epochs + 1e-10)
                            step = cdz_train_params[0]*(cdz_train_params[1]/cdz_train_params[0])**(st)
                            correction = step*lat_step / old_sum
                            if normalise_lat_cdz:
                                correction = correction / torch.sum(lat_step)
                            self.cdz_lat_list[k].connections = self.cdz_lat_list[k].connections + correction
                            if with_inhibition:
                                if cut_inhibition:
                                    self.cdz_lat_list[k].connections[self.cdz_lat_list[k].connections < 0] = 0
                                else:
                                    m_norm = torch.max(self.cdz_lat_list[k].connections)
                                    self.cdz_lat_list[k].connections = self.cdz_lat_list[k].connections / m_norm
                            new_sum = torch.sum(self.cdz_lat_list[k].connections)
                            self.cdz_lat_list[k].connections = old_sum * self.cdz_lat_list[k].connections / new_sum                    
                            
                if (i+1)%test_each == 0:
                    if X_lbls is not None and labeling:
                        self.labeling(X_lbls, Y_lbls, list_cls, train_soms_params)
                    self.tests.append(self.test(X_test, Y_test, metrics = {'acc'}, **kwargs))
                    
        return 0
                    
    def load_resom_weights(self, filename = "cdz_synapses_mnist_hebb_10", soms_inx = (0,1)):
        
        file = open(os.path.dirname(__file__) + "/../weights/MMSOM/" + filename, "r")
        syn_list = [line.split(" ") for line in file.read().split("\n")]
        file.close()
        
        if self.resom_lat_dict is None:
            self.init_resom_lateral()
            
        self.train_lat_mod = 'resom'
            
        som0_x = self.fesoms[soms_inx[0]].som.som_map.shape[0]   
        som0_y = self.fesoms[soms_inx[0]].som.som_map.shape[1]  
        som1_x = self.fesoms[soms_inx[1]].som.som_map.shape[0]   
        som1_y = self.fesoms[soms_inx[1]].som.som_map.shape[1]  
        for k1 in range(som0_x):
            for k2 in range(som0_y):
                for k3 in range(som1_x):
                    for k4 in range(som1_y):
                        self.resom_lat_dict[soms_inx].connections[k1, k2, k3, k4] = float(syn_list[k1*som0_y + k2][k3*som1_y + k4])
                
                
                
    def labeling(self, X_lbls = None, Y_lbls = None,
                 list_cls = None, params = None, max_only_mode = True,
                 min_max_lat_norm = True, labeling_mode =[ None]):
               
        #################
        ### LABELING ######
        ##################
        if X_lbls is not None:
            if list_cls is None:
                self.labels_list  = np.sort(np.unique(Y_lbls))
            else:
                self.labels_list = np.sort(list_cls)
            self.labels_dict = {}
            for i, label in enumerate(self.labels_list):
                self.labels_dict[label] = i 
                
            self.act_collectors = [] 
            for i in range(len(self.fesoms)):
                x_s = self.fesoms[i].map_size[0]
                y_s = self.fesoms[i].map_size[1]
                self.act_collectors.append(torch.zeros((x_s, y_s, len(self.labels_list)),dtype = float))
                lbls_x = X_lbls[i]
                fe_lbls_x = self.fesoms[i].FE.get_features(lbls_x)
                sigma = params[i][4]
                self.fesoms[i].som.labeling(fe_lbls_x, Y_lbls, sigma = sigma, 
                                  list_cls = self.labels_list)
                
            if 'resom' in labeling_mode:
                lbl_counter = torch.zeros(len(self.labels_list), device = self.device)
                
                for k in range(len(X_lbls[0])):
                    label_index = self.labels_dict[Y_lbls[k]]
                    lbl_counter[label_index] += 1
                    for i in range(len(self.fesoms)):
                        lbls_x = X_lbls[i]
                        x = lbls_x[k]
                        fesom = self.fesoms[i]
                        fesom.som.find_bmu(x)
                        sigma = params[i][4]
                        inp_act = torch.exp(-torch.sqrt(fesom.som.dists2)/sigma)
                        for j in range(i):
                            lat = self.resom_lat_dict[(j, i)]
                            # different of Lyes's, sum in place of max activities for neuroms
                            if max_only_mode:
                                out_act = torch.einsum('abcd,cd->abcd', lat.connections, inp_act)
                                out_act, _ = torch.max(out_act, 3)
                                out_act, _ = torch.max(out_act, 2)
                            else:
                                out_act = torch.einsum('abcd,cd->ab', lat.connections, inp_act)
                            if min_max_lat_norm:
                                out_act = out_act / torch.max(out_act)
                            collector = self.act_collectors[j]
                            collector[:,:,label_index] += out_act
                        for j in range(i + 1, len(self.fesoms)):
                            lat = self.resom_lat_dict[(i, j)]
                            if max_only_mode:
                                out_act = torch.einsum('ab,abcd->abcd', inp_act, lat.connections)
                                out_act, _ = torch.max(out_act, 1)
                                out_act, _ = torch.max(out_act, 0)
                            else:
                                out_act = torch.einsum('ab,abcd->cd', inp_act, lat.connections)
                            if min_max_lat_norm:
                                out_act = out_act / torch.max(out_act)
                            collector = self.act_collectors[j]
                            collector[:,:,label_index] += out_act
                            
                for i in range(len(self.fesoms)):
                    collector = torch.einsum('abc,c->abc', 
                                             self.act_collectors[i] ,
                                             1 / lbl_counter)
                    self.fesoms[i].som.accumulator = torch.einsum('abc,abc->abc', 
                                                                  self.fesoms[i].som.accumulator,
                                                                  collector)
                    self.fesoms[i].som.lbl_probs = self.fesoms[i].som.accumulator
                    _, self.fesoms[i].som.lbl_idx = torch.max(self.fesoms[i].som.accumulator, 2)  
                    
            if 'cdz' in labeling_mode: 
                
                lbl_counter = torch.zeros(len(self.labels_list), device = self.device)
                cdz_size = len(self.cdz_lat_list[0].connections.shape) - 2
                collector = torch.zeros((len(self.labels_list), ) + self.cdz_lat_list[0].connections.shape[2:], 
                                        dtype = torch.float,
                                        device = self.device)
                collector_small = torch.ones(self.cdz_lat_list[0].connections.shape[2:], 
                                        dtype = torch.float,
                                        device = self.device)
                self.cdz_bmu_counter = []
                 
                for k in range(len(X_lbls[0])):
                    label_index = self.labels_dict[Y_lbls[k]]
                    lbl_counter[label_index] += 1
                    collector_small[:] = 1
                    for i in range(len(self.fesoms)):
                        x_fe = self.fesoms[i].FE.get_features(X_lbls[i][k:k+1])
                        self.fesoms[i].som.find_bmu(x_fe[0])
                        sigma = params[i][4]
                        
                        act = torch.exp(-torch.sqrt(self.fesoms[i].som.dists2)/sigma)
                        
                        needed_inx = self.possible_indexes[:cdz_size]
                        lat_act = torch.einsum('ab,ab' + needed_inx + '->' + needed_inx,
                                               act,
                                               self.cdz_lat_list[i].connections)
                        
                        collector_small = collector_small*lat_act
                        self.cdz_bmu_counter.append(torch.argmax(lat_act).cpu().detach().numpy())
                        
                    force = ((collector_small - torch.quantile(collector_small, self.cdz_train_q)) /
                        torch.std(collector_small))
                    force = torch.sigmoid(force*self.cdz_train_sig_froce)
                    collector[label_index] = collector[label_index] + force
                    
                for k in range(len(lbl_counter)):
                    collector[k] = collector[k] / lbl_counter[k]
                self.cdz_probs = collector
                _, self.cdz_lbl_idx = torch.max(self.cdz_probs, 0) 
                    
                    
                    
    def predict_proba(self, X, verbose = 1, lat_importance = 1, 
                       multiplication_mode = False, 
                       max_only_mode = False, test_sigma = (1, 1, 1, 1),
                       only_bmu = False, norm_lateral = False, 
                       cdz_resom_ratio = 1,
                       som_scale = (1,1,1,1), **kwargs):
        
        probas = np.zeros((len(X[0]), len(self.labels_list)), dtype = float)
        
        if 'resom' in self.train_lat_mod:
            itr = range(len(X[0]))
            if verbose > 2:
                itr = tqdm(range(len(X[0])))
            if verbose > 1.5:
                print("Given sigmas are", test_sigma)
            for k in tqdm(itr):
                alph_act = []
                beta_act = []
                bmu_s = []
                # count SOM activities
                for i in range(len(self.fesoms)):
                    fesom = self.fesoms[i]
                    x = fesom.FE.get_features(X[i][k:k+1], **kwargs)[0]
                    fesom.som.find_bmu(x)
                    sigma = test_sigma[i]
                    inp_act = torch.exp(-torch.sqrt(fesom.som.dists2)/sigma)*som_scale[i]
                    bmu_act = torch.max(inp_act)
                    bmu_s.append(torch.argmax(inp_act).cpu().detach().numpy())
                    wmu_act = torch.min(inp_act)
                    if not only_bmu:
                        inp_act = (inp_act - wmu_act) / (bmu_act - wmu_act + 1e-10)
                    alph_act.append(inp_act)
                    x_s = self.fesoms[i].map_size[0]
                    y_s = self.fesoms[i].map_size[1]
                    if multiplication_mode:
                        beta_act.append(inp_act)
                    else:
                        beta_act.append(torch.ones((x_s, y_s), device = self.device))
                # count lateral commong activities
                for i in range(len(self.fesoms)):
                    a_act = alph_act[i]
                    if norm_lateral:
                        max_act = torch.max(a_act)
                        min_act = torch.min(a_act)
                        a_act = (a_act - min_act) / (max_act - min_act + 1e-10)
                    a_act = a_act*som_scale[i]
                    for j in range(i):
                        lat = self.resom_lat_dict[(j, i)]
                        # sum normalisaton
                        if max_only_mode:
                            out_act = torch.einsum('abcd,cd->abcd', lat.connections, a_act)                
                            out_act, _ = torch.max(out_act, 3)
                            out_act, _ = torch.max(out_act, 2)
                        else:
                            out_act = torch.einsum('abcd,cd->ab', lat.connections, a_act)
                        beta_act[j] = beta_act[j]*out_act
                    for j in range(i + 1, len(self.fesoms)):
                        lat = self.resom_lat_dict[(i, j)]
                        if max_only_mode:
                            out_act = torch.einsum('ab,abcd->abcd', a_act, lat.connections)
                            out_act, _ = torch.max(out_act, 1)
                            out_act, _ = torch.max(out_act, 0)
                        else:
                            out_act = torch.einsum('ab,abcd->cd', a_act, lat.connections)
                        beta_act[j] = beta_act[j]*out_act
                        
                for i in range(len(self.fesoms)):
                    if multiplication_mode:
                        pass
                    else:
                        beta_act[i] = beta_act[i]*lat_importance + alph_act[i]
                 # find biggest neuron
                max_in_soms = []
                for i in range(len(self.fesoms)):
                    if only_bmu:
                        l = self.fesoms[i].som.map_size[1]
                        max_in_soms.append(beta_act[i][bmu_s[i]//l][bmu_s[i]%l])
                    else:
                        max_in_soms.append(torch.max(beta_act[i]))
                chosen_som = np.argmax(max_in_soms)
                if only_bmu:
                    chosen_som_bmu = bmu_s[chosen_som]
                else:
                    chosen_som_bmu = torch.argmax(beta_act[chosen_som]).cpu().detach().numpy()
                k1 = chosen_som_bmu // self.fesoms[chosen_som].som.map_size[1]
                k2 = chosen_som_bmu % self.fesoms[chosen_som].som.map_size[1]
                pb_for_best = self.fesoms[chosen_som].som.lbl_probs[k1,k2].cpu().detach().numpy()
                #print(chosen_som, chosen_som_bmu, np.argmax(pb_for_best))
                probas[k] = probas[k] + pb_for_best
                
        if 'cdz' in self.train_lat_mod:
            cdz_size = len(self.cdz_lat_list[0].connections.shape) - 2
            needed_inx = self.possible_indexes[:cdz_size]
            cdz_probs = self.cdz_probs.cpu().detach().numpy()
             
            for k in range(len(X[0])):
                if multiplication_mode:
                    predict_collector = torch.ones((self.cdz_lat_list[0].connections.shape[2:]), 
                                    dtype = float, device = self.device)
                else:
                    predict_collector = torch.zeros((self.cdz_lat_list[0].connections.shape[2:]), 
                                    dtype = float, device = self.device)
                    
                for i in range(len(self.fesoms)):
                    x_fe = self.fesoms[i].FE.get_features(X[i][k:k+1])
                    self.fesoms[i].som.find_bmu(x_fe[0])
                    sigma = test_sigma[i]
                    act = torch.exp(-torch.sqrt(self.fesoms[i].som.dists2)/sigma)
                    
                    lat_act = torch.einsum('ab,ab' + needed_inx + '->' + needed_inx,
                                           act,
                                           self.cdz_lat_list[i].connections)
                    if multiplication_mode:
                        predict_collector = predict_collector*lat_act
                    else:
                        predict_collector = predict_collector + lat_act 
                force = ((predict_collector - torch.quantile(predict_collector, self.cdz_train_q)) /
                    torch.std(predict_collector))
                force = torch.sigmoid(force*self.cdz_train_sig_froce).type(torch.FloatTensor)

                probs_selector = torch.einsum('a' + needed_inx + ','+ needed_inx  + '->a',
                       self.cdz_probs,
                       force)
                probas[k] = probas[k] + probs_selector.cpu().detach().numpy()
        
        return probas
    
    # the predict method must give objects as they are (not probas)
    def predict(self, X, multiplication_mode = True,
             lat_importance = 0.1, **kwargs):
        probs =  self.predict_proba(X, multiplication_mode = multiplication_mode,
                               lat_importance = lat_importance, **kwargs)
        self.lbl_idx = probs.argmax(axis = 1)
        prediction = []
        for i in range(len(self.lbl_idx)):
            prediction.append(self.labels_list[self.lbl_idx[i]])
        return prediction
    
    def test(self, X, Y, metrics = {'acc'}, multiplication_mode = True,
             lat_importance = 0.1, **kwargs):
        self.pred_lbl_idx = self.predict(X, multiplication_mode = multiplication_mode,
                               lat_importance = lat_importance, **kwargs)
        out_metrics =  []
        if 'acc' in metrics:
            counter = 0
            for i, y in enumerate(Y):
                if y == self.pred_lbl_idx[i]:
                    counter += 1
            out_metrics.append(counter/len(Y))
        if 'conf_matr' in metrics:
            self.conf_matr = np.zeros((len(np.unique(Y)),len(np.unique(Y))))
            for i, y in enumerate(Y):
                di = self.fesoms[0].som.labels_dict
                self.conf_matr[self.lbl_idx[i],di[y]] = self.conf_matr[self.lbl_idx[i],di[y]] + 1
            out_metrics.append(self.conf_matr)
        return out_metrics
    
 #%%   

# class tests 
if __name__ == "__main__":
    db_folder = os.path.dirname(__file__) + "/../../Databases/New_MNIST_SpokenDigits_databse"
    n_train = 60000
    n_test = 10000
    
    im_x_train_all = np.load(db_folder + "/data_wr_train.npy", allow_pickle=True)
    sp_x_train_all = np.load(db_folder + "/data_sp_train.npy", allow_pickle=True)
    index_train_all = np.load(db_folder + "/labels_train.npy", allow_pickle=True)
    im_x_test_all = np.load(db_folder + "/data_wr_test.npy", allow_pickle=True)
    sp_x_test_all = np.load(db_folder + "/data_sp_test.npy", allow_pickle=True)
    index_test_all = np.load(db_folder + "/labels_test.npy", allow_pickle=True)
    
    im_x_train = np.copy(im_x_train_all[:n_train])
    sp_x_train = np.copy(sp_x_train_all[:n_train])
    index_train = np.copy(index_train_all[:n_train])
    im_x_label = np.copy(im_x_train_all[55000:55000+n_train,:])
    sp_x_label = np.copy(sp_x_train_all[55000:55000+n_train,:])
    index_label = np.copy(index_train_all[55000:55000+n_train])
    im_x_test = np.copy(im_x_test_all[:n_test])
    sp_x_test = np.copy(sp_x_test_all[:n_test])
    index_test = np.copy(index_test_all[:n_test])
    
    #%%  
    #Reshape
    im_size = np.sqrt(im_x_train.shape[1]).astype(int)
    im_x_train_reshaped = np.zeros((im_x_train.shape[0], 3, im_size, im_size))
    im_x_label_reshaped = np.zeros((im_x_label.shape[0], 3, im_size, im_size))
    im_x_test_reshaped = np.zeros((im_x_test.shape[0], 3, im_size, im_size))
    
    im_x_train_reshaped[:,:,:,:] = im_x_train.reshape((im_x_train.shape[0], 1, im_size, im_size))
    im_x_label_reshaped[:,:,:,:] = im_x_label.reshape((im_x_label.shape[0], 1, im_size, im_size))
    im_x_test_reshaped[:,:,:,:] = im_x_test.reshape((im_x_test.shape[0], 1, im_size, im_size))
    