#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  8 18:09:32 2021

@author: aravilievich
"""
import sys
import os

sys.path.append(os.path.dirname(__file__))
from abstract_model import AbstractModel

sys.path.append(os.path.dirname(__file__) + "/../blocks")
from SOM import SOM

sys.path.append(os.path.dirname(__file__) + "/../blocks/FE")
from abstract_FE import Abstract_FE
from empty_FE import Empty_FE

import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm
import math
#%%

class FE_SOM_Model(AbstractModel):

    def __init__(self, **kwargs):     
    # For this function you may define the SOM size, random seed and chose to use gpu or not;
    # And you have to define a concrete SOM vector size (elseway it will be 784 as for MNIST dataset)
        if "load" in kwargs:
            if kwargs["load"]:
                super().__init__(**kwargs)
                return None  
                                       
        if "map_size" in kwargs:
            self.map_size = kwargs["map_size"]
        else:
            self.map_size = (10, 10)
            
        if "vector_len" in kwargs:
            self.vector_len = kwargs["vector_len"]
        else:
            self.vector_len = 784
            
        if "seed" in kwargs:
            self.seed = kwargs["seed"]
        else:
            self.seed = None
            
        if "use_gpu" in kwargs:
            self.use_gpu = kwargs["use_gpu"]
        else:
            self.use_gpu = True
            
        if "load_weights" in kwargs:
            load_weights = kwargs["load_weights"]
        else:
            load_weights = False
            
        if "file_name" in kwargs:
            file_name = kwargs["file_name"]
        else:
            file_name = "/../weights/som/ksom_weights_image_36"
            
        if "file_format" in kwargs:
            file_format = kwargs["file_format"]
        else:
            file_format = "1d_text"
            
        if "som_type" in kwargs:
            som_type = kwargs["som_type"]
        else:
            som_type = "KSOM"
            
        if load_weights:
            self.som = SOM(from_file=True, seed = self.seed, use_gpu = self.use_gpu,
                           file_name = file_name, file_format = file_format, som_type=som_type)
        else:
            self.som = SOM(map_size = self.map_size, vector_len = self.vector_len,
                           seed = self.seed, use_gpu = self.use_gpu, som_type=som_type)
            
        self.map_size = self.som.map_size
        self.vector_len = self.som.vector_len
        
        if "FE_type" in kwargs:
            self.FE_type = kwargs["FE_type"]
        else:
            self.FE_type = "None"
            
        if "loadFE" in kwargs:
            loadFE = kwargs["loadFE"]
        else:
            loadFE = False
            
        if "loadFE_filename" in kwargs:
            loadFE_filename = kwargs["loadFE_filename"]
        else:
            loadFE_filename = None
            
        if 'cnn_type' in kwargs:
            self.cnn_type = kwargs["cnn_type"]
        else:
            self.cnn_type = "resnet50"
            
        if 'use_pretrained' in kwargs:
            self.use_pretrained = kwargs["use_pretrained"]
        else:
            self.use_pretrained = True
            
        if self.FE_type == "None":
            self.FE = Empty_FE(load = loadFE, filename = loadFE_filename)
            
    # given prams must be epsilon_i, epsilon_f, eta_i, eta_f 
    # - the beggining and final eps and eta, changing during epochs
    def train(self, X, Y = None, epochs = 10, verbose = 2, params_evolution = "auto",
              params = (1, 0.01, 10, 0.01), sigma = 1.0, custom_lbls = False,
              lbls_procent = 0.01, skip_train = False, skip_labeling = False,
              evol_each_obj = False, batch_size = 50, start_fe_l_on_ep = 2,
              X_lbls = None, Y_lbls = None, list_cls = None, 
              save_best = False, save_filename = "tmp",
              X_test = None, Y_test = None, 
              shake = False, test_each_n = None, seed = None, **kwargs):
        
        self.epochs = epochs
        self.params_evolution = params_evolution
        self.train_params = params 
        self.sigma = sigma
            
        self.custom_lbls = custom_lbls
        self.lbls_procent = lbls_procent
        self.batch_size = batch_size
        
        self.test_each_n = test_each_n
        if test_each_n is None:
            self.test_each_n = math.ceil(len(X)/self.batch_size)
            
        self.list_cls = list_cls
        self.save_best = save_best 
        self.save_filename = save_filename
            
        eps_i = self.train_params[0]
        eps_f = self.train_params[1]
        eta_i = self.train_params[2]
        eta_f = self.train_params[3]
        self.acc_list = []
        self.metrics_list = []
        
        if not skip_train:
            for ep in range(self.epochs):
                if shake:
                    if seed is not None:
                        np.random.seed(seed)
                    new_order = np.random.permutation(len(X))
                    X = X[new_order]
                    if Y is not None:
                        Y = Y[new_order]
                if verbose > 1:
                    print("\n Start of epoch", ep + 1) 
                if self.params_evolution == "auto":
                    eta = eta_i*(eta_f/eta_i)**(ep/(self.epochs - 1 + 1e-10))
                    eps = eps_i*(eps_f/eps_i)**(ep/(self.epochs - 1 + 1e-10))
                if verbose > 1:
                    print(eps, eta, "\n")
                num_steps = math.ceil(len(X)/self.batch_size)
                if verbose > 2:
                    X_iterator = tqdm(range(num_steps))
                else:
                    X_iterator = range(num_steps)
                for k in X_iterator:  
                    x = X[k*self.batch_size:(k+1)*self.batch_size]
                    x_fe = self.FE.get_features(x, **kwargs)
                    st = (ep + k /num_steps) / (self.epochs + 1e-10)
                    if (evol_each_obj) and self.params_evolution == "auto": 
                        eta = eta_i*(eta_f/eta_i)**(st)
                        eps = eps_i*(eps_f/eps_i)**(st)
                    self.som.train_one_epoch(x_fe, epsilon = eps, eta = eta, verbose = verbose) 
                    if (k+1) % self.test_each_n == 0: #and start_fe_l_on_ep <= ep + k /num_steps:
                        if self.custom_lbls:
                            lbls_x = X_lbls
                            lbls_y = Y_lbls
                        else:
                            lbls_x = X[:int(len(X)*self.lbls_procent)]
                            lbls_y = Y[:int(len(X)*self.lbls_procent)]
                        fe_lbls_x = self.FE.get_features(lbls_x)
                        self.som.labeling(fe_lbls_x, lbls_y, sigma = self.sigma, 
                                          list_cls = self.list_cls)
                        metrics = self.test(X_test, Y_test)
                        acc = metrics[0]
                        self.acc_list.append(acc)
                        self.metrics_list.append(metrics)
                        if self.save_best and acc == np.max(self.acc_list):
                            self.save(filename = self.save_filename)
                        if verbose > 2.5:
                            print(acc)
                if verbose > 1.8:
                    print("Best acc to the epochs",ep, " is ", np.max(self.acc_list))
        
        if not skip_labeling:
            if self.custom_lbls:
                lbls_x = X_lbls
                lbls_y = Y_lbls
            else:
                lbls_x = X[:int(len(X)*self.lbls_procent)]
                lbls_y = Y[:int(len(X)*self.lbls_procent)]
            fe_lbls_x = self.FE.get_features(lbls_x)
            self.som.labeling(fe_lbls_x, lbls_y, sigma = self.sigma, list_cls = self.list_cls)    

    def predict_proba(self, X, **kwargs):
        return self.som.predict_proba(X)
    
    # the predict method must give objects as they are (not probas)
    def predict(self, X, **kwargs):
        X_F = self.FE.get_features(X)
        return self.som.predict(X_F)
    
    def test(self, X, Y, metrics = ('acc', 'som_mse')):
        X_F = self.FE.get_features(X)
        acc, self.c_m = self.som.test(X_F, Y)
        return acc
    
 #%%   

# class tests 
if __name__ == "__main__":
    db_folder = os.path.dirname(__file__) + "/../../Databases/New_MNIST_SpokenDigits_databse"
    n_train = 60000
    n_test = 10000
    
    im_x_train_all = np.load(db_folder + "/data_wr_train.npy", allow_pickle=True)
    sp_x_train_all = np.load(db_folder + "/data_sp_train.npy", allow_pickle=True)
    index_train_all = np.load(db_folder + "/labels_train.npy", allow_pickle=True)
    im_x_test_all = np.load(db_folder + "/data_wr_test.npy", allow_pickle=True)
    sp_x_test_all = np.load(db_folder + "/data_sp_test.npy", allow_pickle=True)
    index_test_all = np.load(db_folder + "/labels_test.npy", allow_pickle=True)
    
    im_x_train = np.copy(im_x_train_all[:n_train])
    sp_x_train = np.copy(sp_x_train_all[:n_train])
    index_train = np.copy(index_train_all[:n_train])
    im_x_label = np.copy(im_x_train_all[55000:55000+n_train,:])
    sp_x_label = np.copy(sp_x_train_all[55000:55000+n_train,:])
    index_label = np.copy(index_train_all[55000:55000+n_train])
    im_x_test = np.copy(im_x_test_all[:n_test])
    sp_x_test = np.copy(sp_x_test_all[:n_test])
    index_test = np.copy(index_test_all[:n_test])
    
    #%%  
    #Reshape
    im_size = np.sqrt(im_x_train.shape[1]).astype(int)
    im_x_train_reshaped = np.zeros((im_x_train.shape[0], 3, im_size, im_size))
    im_x_label_reshaped = np.zeros((im_x_label.shape[0], 3, im_size, im_size))
    im_x_test_reshaped = np.zeros((im_x_test.shape[0], 3, im_size, im_size))
    
    im_x_train_reshaped[:,:,:,:] = im_x_train.reshape((im_x_train.shape[0], 1, im_size, im_size))
    im_x_label_reshaped[:,:,:,:] = im_x_label.reshape((im_x_label.shape[0], 1, im_size, im_size))
    im_x_test_reshaped[:,:,:,:] = im_x_test.reshape((im_x_test.shape[0], 1, im_size, im_size))
    