#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  8 18:09:32 2021

@author: aravilievich
"""
import sys
import os

sys.path.append(os.path.dirname(__file__))
from abstract_model import AbstractModel
sys.path.append(os.path.dirname(__file__) + "/../blocks")
from SOM import SOM
import numpy as np
import matplotlib.pyplot as plt
 #%% 

class SOM_Model(AbstractModel):

    def __init__(self, **kwargs):     
    # For this function you may define the SOM size, random seed and chose to use gpu or not;
    # And you have to define a concrete SOM vector size (elseway it will be 784 as for MNIST dataset)
        if "load" in kwargs:
            if kwargs["load"]:
                #kwarg model_filename used for creation
                super().__init__(**kwargs)
                return None  
                                            
        if "map_size" in kwargs:
            self.map_size = kwargs["map_size"]
        else:
            self.map_size = (10, 10)
            
        if "vector_len" in kwargs:
            self.vector_len = kwargs["vector_len"]
        else:
            self.vector_len = 784
            
        if "seed" in kwargs:
            self.seed = kwargs["seed"]
        else:
            self.seed = None
            
        if "use_gpu" in kwargs:
            self.use_gpu = kwargs["use_gpu"]
        else:
            self.use_gpu = True
            
        if "load_weights" in kwargs:
            load_weights = kwargs["load_weights"]
        else:
            load_weights = False
            
        if "file_name" in kwargs:
            file_name = kwargs["file_name"]
        else:
            file_name = "/../weights/som/ksom_weights_image_36"
            
        if "file_format" in kwargs:
            file_format = kwargs["file_format"]
        else:
            file_format = "1d_text"
            
        if "som_type" in kwargs:
            som_type = kwargs["som_type"]
        else:
            som_type = "KSOM"
            
        if load_weights:
            self.som = SOM(from_file=True, seed = self.seed, use_gpu = self.use_gpu,
                           file_name = file_name, file_format = file_format)
        else:
            self.som = SOM(map_size = self.map_size, vector_len = self.vector_len,
                           seed = self.seed, use_gpu = self.use_gpu, som_type = som_type)
            
        self.map_size = self.som.map_size
        self.vector_len = self.som.vector_len
    
    # given prams must be epsilon_i, epsilon_f, eta_i, eta_f 
    # - the beggining and final eps and eta, changing during epochs
    def train(self, X, Y = None, **kwargs):
        
        if "epochs" in kwargs:
            self.epochs = kwargs["epochs"]
        else:
            self.epochs = 10
            
        if "verbose" in kwargs:
            verbose = kwargs["verbose"]
        else:
            verbose = 2   
       
        if "params_evolution" in kwargs:
            self.params_evolution = kwargs["params_evolution"]
        else:
            self.params_evolution = "auto"
            
        if "params" in kwargs:
            self.train_params = kwargs["params"]
        else:
            self.train_params = (1, 0.01, 10, 0.01)
            
        if "sigma" in kwargs:
            self.sigma = kwargs["sigma"]
        else:
            self.sigma = 1.0
            
        if "custom_lbls" in kwargs:
            self.custom_lbls = kwargs["custom_lbls"]
        else:
            self.custom_lbls = False
            
        if "lbls_procent" in kwargs:
            self.lbls_procent = kwargs["lbls_procent"]
        else:
            self.lbls_procent = 0.01
            
        if "skip_train" in kwargs:
            skip_train = kwargs["skip_train"]
        else:
            skip_train = False
            
        if "skip_labeling" in kwargs:
            skip_labeling = kwargs["skip_labeling"]
        else:
            skip_labeling = False
            
        if "evol_each_obj" in kwargs:
            evol_each_obj = kwargs["evol_each_obj"]
        else:
            evol_each_obj = False
            
        if "X_lbls" in kwargs:
            self.X_lbls = kwargs["X_lbls"]
        if "Y_lbls" in kwargs:
            self.Y_lbls = kwargs["Y_lbls"]
        
        if not skip_train:
            self.som.train(X, n_epochs=self.epochs, verbose = verbose,
                       params_evolution = self.params_evolution,
                       epsilon_i = self.train_params[0],
                       epsilon_f = self.train_params[1],
                       eta_i = self.train_params[2],
                       eta_f = self.train_params[3],
                       evol_each_obj = evol_each_obj)
        
        if not skip_labeling:
            if self.custom_lbls:
                lbls_x = self.X_lbls
                lbls_y = self.Y_lbls
            else:
                lbls_x = X[:int(len(X)*self.lbls_procent)]
                lbls_y = Y[:int(len(X)*self.lbls_procent)]
            self.som.labeling(lbls_x, lbls_y, sigma= self.sigma)

    def predict_proba(self, X, **kwargs):
        return self.som.predict_proba(X)
    
    # the predict method must give objects as they are (not probas)
    def predict(self, X, **kwargs):
        return self.som.predict(X)
    
    def test(self, X, Y):
        acc, c_m = self.som.test(X, Y)
        return acc
    
 #%%   

# class tests 
if __name__ == "__main__":
    db_folder = os.path.dirname(__file__) + "/../../Databases/New_MNIST_SpokenDigits_databse"
    n_train = 60000
    n_test = 10000
    
    im_x_train_all = np.load(db_folder + "/data_wr_train.npy", allow_pickle=True)
    sp_x_train_all = np.load(db_folder + "/data_sp_train.npy", allow_pickle=True)
    index_train_all = np.load(db_folder + "/labels_train.npy", allow_pickle=True)
    im_x_test_all = np.load(db_folder + "/data_wr_test.npy", allow_pickle=True)
    sp_x_test_all = np.load(db_folder + "/data_sp_test.npy", allow_pickle=True)
    index_test_all = np.load(db_folder + "/labels_test.npy", allow_pickle=True)
    
    im_x_train = np.copy(im_x_train_all[:n_train])
    sp_x_train = np.copy(sp_x_train_all[:n_train])
    index_train = np.copy(index_train_all[:n_train])
    im_x_label = np.copy(im_x_train_all[55000:55000+n_train,:])
    sp_x_label = np.copy(sp_x_train_all[55000:55000+n_train,:])
    index_label = np.copy(index_train_all[55000:55000+n_train])
    im_x_test = np.copy(im_x_test_all[:n_test])
    sp_x_test = np.copy(sp_x_test_all[:n_test])
    index_test = np.copy(index_test_all[:n_test])
    
    #%%  
    model = SOM_Model(vector_len = 784, seed = 0, use_gpu = False)
    model.train(im_x_train, index_train, params = (1, 0.01, 10, 0.01), sigma = 1.0, lbls_procent = 0.01, epochs = 8, verbose = 3)
    acc, c_m = model.test(im_x_test, index_test)
    print(acc)
    plt.imshow(c_m)
      
    #%%  
    model.train(im_x_train, index_train, skip_train = True, params = (1, 0.01, 10, 0.01), sigma = 1.0, lbls_procent = 0.001, verbose = 2)
    acc, c_m = model.test(im_x_test, index_test)
    print(acc)
    plt.imshow(c_m)
    
    #%%  
    model.train(im_x_train, index_train, skip_train = True, custom_lbls = True, X_lbls = im_x_label, Y_lbls = index_label, verbose = 3)
    acc, c_m = model.test(im_x_test, index_test)
    print(acc)
    plt.imshow(c_m)
    
    #%%  
    model = SOM_Model(load_weights = True, seed = 0)
    model.train(im_x_train, index_train, skip_train = True, custom_lbls = True, X_lbls = im_x_label, Y_lbls = index_label, verbose = 3)
    acc, c_m = model.test(im_x_test, index_test)
    print(acc)
    plt.imshow(c_m)
    #%%  
    print(model.common_test(im_x_test, index_test))
    #%% 
    ########### 
    # Test on spoken MNIST
    #########     
    #%%  
    model = SOM_Model(map_size = (16,16), vector_len = 507, seed = 0, use_gpu = False)
    model.train(sp_x_train, index_train, params = (1, 0.01, 10, 0.01), sigma = 0.1, lbls_procent = 0.01, epochs = 8, verbose = 3)
    acc, c_m = model.test(sp_x_test, index_test)
    print(acc)
    plt.imshow(c_m)
