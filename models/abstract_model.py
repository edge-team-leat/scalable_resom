#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  8 18:09:32 2021

@author: aravilievich
"""

from abc import ABC, abstractmethod
import os
import pickle
FILES_FOLDER = os.path.dirname(__file__) + "/../weights/models/"

class AbstractModel(ABC):
    
    def __init__(self, **kwargs):
        if "load" in kwargs:
            if kwargs["load"]:
                loaded = pickle.load( open(FILES_FOLDER + kwargs["model_filename"] + ".pickle", "rb" ) )
                self.__dict__.update(loaded.__dict__)
    
    @abstractmethod
    def train(self, X, **kwargs):
        pass

    @abstractmethod
    def predict_proba(self, X, **kwargs):
        pass
    
    # the predict method must give objects as they are (not probas)
    @abstractmethod
    def predict(self, X, **kwargs):
        pass
    
    @abstractmethod
    def test(self, X, Y, **kwargs):
        pass
    
    def evaluate(self, Y_true, Y_pred, method = 'acc'):
        score = None
        if method == 'acc':
            counter = 0
            for i in range(len(Y_true)):
                if Y_true[i] == Y_pred[i]:
                    counter = counter + 1
            score = counter / len(Y_true)
        return score
    
    def common_test(self, X, Y, **kwargs):
        Y_pred = self.predict(X, **kwargs)
        method = 'acc'
        if "method" in kwargs:
            method = kwargs["method"]
        return self.evaluate(Y, Y_pred, method)
    
    def save(self, filename = "tmp_FE"):
        pickle.dump(self, file = open(FILES_FOLDER + filename + ".pickle", "wb" ) )
        return