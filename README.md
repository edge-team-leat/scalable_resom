### What is this repository for? ###

* The repository concerns to the simulation of the Scalable ReSOM framework, presented in the article "A unified software/hardware scalable architecture for brain-inspired computing based on self-organizing neural models" which is in the process of publishing for the Frontiers in Neuroscience.

### How do I get set up? ###

* To launch the tests you need to go into the folder /tests/
* In the file "tests_description.txt" you can find short description for the tests
* In the in other 3 files you can find the python code to see the model's behavior
* The tests Databases (The datasets analyzed in this study can be found in : MNIST and Speech Commands - (Khacef et al., 2020d), FMNIST - (Xiao et al., 2017), and hand signs dataset - (Mavi, 2021)) can be found in the foulde /Databases/ 
* All other fouldes are contain code and pretrained weights for the model. 

### Who do I talk to? ###

* Created by PhD student Artem Muliukov (Univertite Cote d'Azur, 3IA Cote d'Azur funding)
* Co-administrated by Laurent Rodriguez and Benoit Miramond
* The core team location is the Laboratory of Electronics Antennas and Telecommunications (a Joint loboratory of the University of Nice-Sophia Antipolis and the CNRS, France). 

### Rerefences: ###

* [Dataset] Xiao, H., Rasul, K., and Vollgraf, R. (2017). Fashion-mnist: a novel image dataset for benchmarking machine learning algorithms

* [Dataset] Mavi, A. (2021). A new dataset and proposed convolutional neural network architecture for classification of american sign language digits

* [Dataset] Khacef, L., Rodriguez, L., and Miramond, B. (2020d). Written and spoken digits database for multimodal learning. doi:10.5281/zenodo.4452953

