#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 24 17:55:13 2021

@author: aravilievich
"""

import sys
import os
sys.path.append(os.path.dirname(__file__) + "/../models")
from som_model import SOM_Model
from fe_som_model import FE_SOM_Model
sys.path.append(os.path.dirname(__file__) + "/../blocks/FE")
from abstract_FE import Abstract_FE
from empty_FE import Empty_FE

import numpy as np
from tqdm import tqdm
import matplotlib.pyplot as plt
import time
import optuna

#%%
if __name__ == "__main__":
    
    #########################
    ###### Take data##########
    ###########################
    
    db_folder = os.path.dirname(__file__) + "/../Databases/New_MNIST_SpokenDigits_databse"
    n_label = 5000
    n_test = 10000
    
    im_x_train_all = np.load(db_folder + "/data_wr_train.npy", allow_pickle=True)
    sp_x_train_all = np.load(db_folder + "/data_sp_train.npy", allow_pickle=True)
    index_train_all = np.load(db_folder + "/labels_train.npy", allow_pickle=True)
    im_x_test_all = np.load(db_folder + "/data_wr_test.npy", allow_pickle=True)
    sp_x_test_all = np.load(db_folder + "/data_sp_test.npy", allow_pickle=True)
    index_test_all = np.load(db_folder + "/labels_test.npy", allow_pickle=True)
    
    im_x_train = np.copy(im_x_train_all[:])
    sp_x_train = np.copy(sp_x_train_all[:])
    index_train = np.copy(index_train_all[:])
    im_x_label = np.copy(im_x_train_all[55000:55000+n_label,:])
    sp_x_label = np.copy(sp_x_train_all[55000:55000+n_label,:])
    index_label = np.copy(index_train_all[55000:55000+n_label])
    im_x_test = np.copy(im_x_test_all[:n_test])
    sp_x_test = np.copy(sp_x_test_all[:n_test])
    index_test = np.copy(index_test_all[:n_test])
    
    list_cls = np.unique(index_train)
    
    #%%  
    #test of pretrained FE with no learning
    ##############
    model1 = FE_SOM_Model(vector_len = 784, seed = 0, use_gpu = False, FE_type = "None",
                         loadFE = False, map_size = (10,10), som_type = "KSOM")
    
    #%% 
    model1.train(im_x_train, index_train, params = (0.5, 0.003, 1.5, 0.002), sigma = 0.245, 
                epochs = 10, verbose = 3, evol_each_obj = True,
                custom_lbls = True, X_lbls = im_x_label, Y_lbls = index_label, shake = True,
                X_test = im_x_test, Y_test = index_test, test_each_n = 100, list_cls = list_cls,
                save_best = True, save_filename = "best_mnist_som")
    #%% 
    ######
    ## TEST BEST trained on mnist ###
    #######
    model1 = FE_SOM_Model(load = True, model_filename = "best_mnist_som")
    #%%
    acc = model1.test(X = im_x_test, Y = index_test, )
    print(acc[0])
    #acc = 0.88
    
    #%%    
    model2 = FE_SOM_Model(vector_len = 507, seed = 0, use_gpu = False, FE_type = "None",
                     loadFE = False, map_size = (16,16), som_type = "KSOM")
    
    #%%
    model2.train(sp_x_train, index_train, params = (0.19235191146472755, 0.00733863310505143,
                                                    2.9945635053038564,  0.013615823089292333),
                 sigma =  0.06953102172752228, 
                epochs = 10, verbose = 3, evol_each_obj = True,
                custom_lbls = True, X_lbls =  sp_x_label, Y_lbls = index_label, shake = True,
                X_test = sp_x_test, Y_test = index_test, test_each_n = 100,
                save_best = True, save_filename = "best_smnist_16som", list_cls = list_cls)
    #%% 
    ######
    ## TEST BEST trained on mnist ###
    #######
    model2 = FE_SOM_Model(load = True, model_filename = "best_smnist_16som")
    #%%
    acc = model2.test(X = sp_x_test, Y = index_test, )
    print('SOM 2 Accuracy', acc[0])
    #acc = 0.76
    
    #%%  
    ############################
    ####################
    # find best for S-MNIST 16x16 MAP
    ###########################
    #########################
    
    def make_one_run_decr(X_tr, X_lbl, Y_lbl, X_te, Y_te,
                 eps_i, eps_f, eta_i, eta_f, sigma, seed = 0):
        model = FE_SOM_Model(vector_len = 507, seed = 0, use_gpu = False, FE_type = "None",
                         loadFE = False, map_size = (16,16), som_type = "KSOM")
        model.train(X_tr, index_train, params = (eps_i, eps_f, eta_i, eta_f), sigma = sigma, 
                    epochs = 10, verbose = 2, evol_each_obj = True,
                    custom_lbls = True, X_lbls =  X_lbl, Y_lbls = Y_lbl, shake = True,
                    X_test = X_te, Y_test = Y_te, test_each_n = 100,
                    save_best = True, save_filename = "temp", list_cls = list_cls)
        model = FE_SOM_Model(load = True, model_filename = "temp")
        acc = model.test(X = sp_x_test, Y = index_test)
        return acc

    def objective(trial):
        eps_i = trial.suggest_loguniform('eps_i', 0.1, 10)
        eps_f = trial.suggest_loguniform('eps_f', 0.001, 1)
        eta_i = trial.suggest_loguniform('eta_i', 0.1, 100)
        eta_f = trial.suggest_loguniform('eta_f', 0.001, 1)
        sigma = trial.suggest_loguniform('sigma', 0.01, 20)
        acc = make_one_run_decr(sp_x_train, sp_x_label, index_label,
                                sp_x_test, index_test,
                                eps_i, eps_f, eta_i, eta_f, sigma)
        return acc[0]
    #%%
    study = optuna.create_study(direction='maximize')
    #%%
    study.optimize(objective, n_trials=1000)
    # Trial 138 finished with value: 0.7643 and parameters: 
    # {'eps_i': 0.19235191146472755, 'eps_f': 0.00733863310505143, 
    # 'eta_i': 2.9945635053038564, 'eta_f': 0.013615823089292333, 'sigma': 0.06953102172752228}. 
    # Best is trial 107 with value: 0.7774.