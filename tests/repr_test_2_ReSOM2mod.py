#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 24 17:55:13 2021

@author: aravilievich
"""

import sys
import os
sys.path.append(os.path.dirname(__file__) + "/../models")
from som_model import SOM_Model
from fe_som_model import FE_SOM_Model
from mmfe_som_model import MMFE_SOM_Model
sys.path.append(os.path.dirname(__file__) + "/../blocks/FE")
from abstract_FE import Abstract_FE
from empty_FE import Empty_FE
#from test_1_1 import  get_image_after_test, make_video

import numpy as np
from tqdm import tqdm
import matplotlib.pyplot as plt
import time
import optuna
import torch

#%%
if __name__ == "__main__":
    
    #########################
    ###### Take data##########
    ###########################
    
    db_folder = os.path.dirname(__file__) + "/../Databases/New_MNIST_SpokenDigits_databse"
    n_label = 5000
    n_test = 10000
    
    im_x_train_all = np.load(db_folder + "/data_wr_train.npy", allow_pickle=True)
    sp_x_train_all = np.load(db_folder + "/data_sp_train.npy", allow_pickle=True)
    index_train_all = np.load(db_folder + "/labels_train.npy", allow_pickle=True)
    im_x_test_all = np.load(db_folder + "/data_wr_test.npy", allow_pickle=True)
    sp_x_test_all = np.load(db_folder + "/data_sp_test.npy", allow_pickle=True)
    index_test_all = np.load(db_folder + "/labels_test.npy", allow_pickle=True)
    
    im_x_train = np.copy(im_x_train_all[:])
    sp_x_train = np.copy(sp_x_train_all[:])
    index_train = np.copy(index_train_all[:])
    im_x_label = np.copy(im_x_train_all[55000:55000+n_label,:])
    sp_x_label = np.copy(sp_x_train_all[55000:55000+n_label,:])
    index_label = np.copy(index_train_all[55000:55000+n_label])
    im_x_test = np.copy(im_x_test_all[:n_test])
    sp_x_test = np.copy(sp_x_test_all[:n_test])
    index_test = np.copy(index_test_all[:n_test])
    
    list_cls = np.unique(index_train)
    
    #%% 
    ######
    ## Retest used SOMs
    #######
    model1 = FE_SOM_Model(load = True, model_filename = "best_mnist_som")
    acc = model1.test(X = im_x_test, Y = index_test, )
    print('SOM 1 Accuracy', acc[0])
    #acc = 0.88
    #%% 
    model2 = FE_SOM_Model(load = True, model_filename = "best_smnist_16som")
    acc = model2.test(X = sp_x_test, Y = index_test, )
    print('SOM 2 Accuracy', acc[0])
    #acc = 0.76
    
    #%%
    #########################################
    #### Get results knowing parameters ########
    ###########################################
    alpha1 = 37.82265671
    alpha2 = 16.337380
    lat_importance = 0.0189961441
    beta1 = 1.802932484
    beta2 = 0.2078495214
    multipl_mode = False
    max_only_mode =  True
    only_bmu = False
    norm_lateral = False
    
    train_soms_params = ((1, 0.01, 10, 0.01, alpha1),
                         (1, 0.01, 10, 0.01, alpha2))
    
    model1 = FE_SOM_Model(load = True, model_filename = "best_mnist_som")
    model2 = FE_SOM_Model(load = True, model_filename = "best_smnist_16som")
    resom_model = MMFE_SOM_Model(take_trained_fesoms = True, fesoms = (model1, model2),
                 use_gpu = False, random_lat_weights = False, seed = 777, 
                 labels_list = model1.som.labels_list)
    resom_model.train(X = (im_x_train, sp_x_train), Y = index_train,
                      train_lateral = True, train_lat_mod = 'resom',
                      resom_train_params =  (1.,),
                      X_lbls = (im_x_label, sp_x_label), Y_lbls = index_label,
                      list_cls = None, labeling = False,
                      train_soms = False, X_test = (im_x_test, sp_x_test), Y_test = index_test,
                      train_soms_params = train_soms_params, 
                      num_epochs = 1, verbose = 3, test_each = 1e10,
                      save_activations = False, ignore_activations = False)     
    acc = resom_model.test((im_x_test, sp_x_test), index_test, 
                   metrics = {'acc'}, multiplication_mode = multipl_mode,
                   lat_importance = lat_importance, test_sigma = (beta1, beta2),
                   verbose = 1, max_only_mode = max_only_mode, 
                   only_bmu = only_bmu, norm_lateral = norm_lateral)

    # Score about 0.966 
    print("2 SOMs model executed with accuracy:", acc)
    #%%  
    ###########
    ##################
    ## How to search parameters 1 ###
    ################
    ################
    alpha1 = 10 
    alpha2 = 10
    multipl_mode = True
    max_only_mode =  True
    only_bmu = True 
    norm_lateral = True 
    
    train_soms_params = ((1, 0.01, 10, 0.01, alpha1),
                         (1, 0.01, 10, 0.01, alpha2))
    
    model1 = FE_SOM_Model(load = True, model_filename = "best_mnist_som")
    model2 = FE_SOM_Model(load = True, model_filename = "best_smnist_16som")
    resom_model = MMFE_SOM_Model(take_trained_fesoms = True, fesoms = (model1, model2),
                 use_gpu = False, random_lat_weights = False, seed = 777, 
                 labels_list = model1.som.labels_list)
    resom_model.train(X = (im_x_train, sp_x_train), Y = index_train,
                      train_lateral = True, train_lat_mod = 'resom',
                      resom_train_params =  (1.,),
                      X_lbls = (im_x_label, sp_x_label), Y_lbls = index_label,
                      list_cls = None, labeling = False,
                      train_soms = False, X_test = (im_x_test, sp_x_test), Y_test = index_test,
                      train_soms_params = train_soms_params, 
                      num_epochs = 1, verbose = 3, test_each = 1e10,
                      save_activations = False, ignore_activations = False)  
    #%%  
    def objective(trial):
        beta1 = trial.suggest_loguniform('beta1', 0.01, 100)
        beta2 = trial.suggest_loguniform('beta2', 0.01, 100)
     
        acc = resom_model.test((im_x_test, sp_x_test), index_test, 
                       metrics = {'acc'}, multiplication_mode = multipl_mode,
                       lat_importance = 1, test_sigma = (beta1, beta2),
                       verbose = 1, max_only_mode = max_only_mode, 
                       only_bmu = only_bmu, norm_lateral = norm_lateral)
        
        return acc
    #%%           
    study1 = optuna.create_study(direction='maximize')
    #%%
    study1.optimize(objective, n_trials=10000) 
    #%%  
    ###########
    ##################
    ## How to search parameters 2 ###
    ################
    ################
    def objective(trial):
        alpha1 = trial.suggest_loguniform('alpha1', 0.01, 100)
        alpha2 = trial.suggest_loguniform('alpha2', 0.01, 100)
        lat_importance = trial.suggest_loguniform('lat_importance', 0.001, 1000)
        beta1 = trial.suggest_loguniform('beta1', 0.01, 100)
        beta2 = trial.suggest_loguniform('beta2', 0.01, 100)
        multipl_mode = trial.suggest_categorical('multipl_mode', (False, True))
        max_only_mode =  trial.suggest_categorical('max_only_mode', (False, True))
        only_bmu = trial.suggest_categorical('only_bmu', (False, True))
        norm_lateral = trial.suggest_categorical('norm_lateral', (False, True))
        
        train_soms_params = ((1, 0.01, 10, 0.01, alpha1),
                             (1, 0.01, 10, 0.01, alpha2))
        
        model1 = FE_SOM_Model(load = True, model_filename = "best_mnist_som")
        model2 = FE_SOM_Model(load = True, model_filename = "best_smnist_16som")
        resom_model = MMFE_SOM_Model(take_trained_fesoms = True, fesoms = (model1, model2),
                     use_gpu = False, random_lat_weights = False, seed = 777, 
                     labels_list = model1.som.labels_list)
        resom_model.train(X = (im_x_train, sp_x_train), Y = index_train,
                          train_lateral = True, train_lat_mod = 'resom',
                          resom_train_params =  (1.,),
                          X_lbls = (im_x_label, sp_x_label), Y_lbls = index_label,
                          list_cls = None, labeling = False,
                          train_soms = False, X_test = (im_x_test, sp_x_test), Y_test = index_test,
                          train_soms_params = train_soms_params, 
                          num_epochs = 1, verbose = 3, test_each = 1e10,
                          save_activations = False, ignore_activations = False)     
        acc = resom_model.test((im_x_test, sp_x_test), index_test, 
                       metrics = {'acc'}, multiplication_mode = multipl_mode,
                       lat_importance = lat_importance, test_sigma = (beta1, beta2),
                       verbose = 1, max_only_mode = max_only_mode, 
                       only_bmu = only_bmu, norm_lateral = norm_lateral)
        
        return acc
    #%%           
    study2 = optuna.create_study(direction='maximize')
    #%%
    study2.optimize(objective, n_trials=10000) 
    #%%   
    """FrozenTrial(number=1964, values=[0.9662], 
    params={'alpha1': 37.82265671918842, 'alpha2': 16.33738079259604, 
            'lat_importance': 0.01899614417939658, 'beta1': 1.8029324846671875, 
            'beta2': 0.20784952143294252, 'multipl_mode': False, 'max_only_mode': True, 
            'only_bmu': False, 'norm_lateral': False}"""
    