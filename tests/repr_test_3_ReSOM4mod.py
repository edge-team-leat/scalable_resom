#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 24 17:55:13 2021

@author: aravilievich
"""

import sys
import os
sys.path.append(os.path.dirname(__file__) + "/../models")
from som_model import SOM_Model
from fe_som_model import FE_SOM_Model
from mmfe_som_model import MMFE_SOM_Model
sys.path.append(os.path.dirname(__file__) + "/../blocks/FE")
from abstract_FE import Abstract_FE
from empty_FE import Empty_FE

import numpy as np
from tqdm import tqdm
import matplotlib.pyplot as plt
import time
import optuna
import torch
import torchvision.datasets as dset
import pandas as pd
import cv2
import seaborn as sn

#%%
if __name__ == "__main__":
    
    #########################
    ###### Take data##########
    ###########################
    
    # MNIST + SMNIST
    db_folder = os.path.dirname(__file__) + "/../Databases/New_MNIST_SpokenDigits_databse"
    n_label = 5000
    n_test = 10000
    
    im_x_train_all = np.load(db_folder + "/data_wr_train.npy", allow_pickle=True)
    sp_x_train_all = np.load(db_folder + "/data_sp_train.npy", allow_pickle=True)
    index_train_all = np.load(db_folder + "/labels_train.npy", allow_pickle=True)
    im_x_test_all = np.load(db_folder + "/data_wr_test.npy", allow_pickle=True)
    sp_x_test_all = np.load(db_folder + "/data_sp_test.npy", allow_pickle=True)
    index_test_all = np.load(db_folder + "/labels_test.npy", allow_pickle=True)
    
    im_x_train = np.copy(im_x_train_all[:])
    sp_x_train = np.copy(sp_x_train_all[:])
    index_train = np.copy(index_train_all[:])
    im_x_label = np.copy(im_x_train_all[55000:55000+n_label,:])
    sp_x_label = np.copy(sp_x_train_all[55000:55000+n_label,:])
    index_label = np.copy(index_train_all[55000:55000+n_label])
    im_x_test = np.copy(im_x_test_all[:n_test])
    sp_x_test = np.copy(sp_x_test_all[:n_test])
    index_test = np.copy(index_test_all[:n_test])
    
    list_cls = np.unique(index_train)
    
 
    #Prepearing to use the FMNIST DATA  
    #%% 
    ######
    db_folder2 = os.path.dirname(__file__) + "/../Databases/Fashion_MNIST"
    train_csv = pd.read_csv(db_folder2 + "/fashion-mnist_train.csv")
    test_csv = pd.read_csv(db_folder2 + "/fashion-mnist_test.csv")
    train_csv_np = train_csv.to_numpy()
    test_csv_np = test_csv.to_numpy()
    #%%
    # Reorder DATA
    pocket_fmnsit_tr = {}
    pocket_fmnsit_te = {}
    for i in range(10):
        pocket_fmnsit_tr[i] = []
        pocket_fmnsit_te[i] = []
    for i in range(train_csv.shape[0]):
        pocket_fmnsit_tr[train_csv_np[i,0]].append(train_csv_np[i,1:] / 255)
    for i in range(test_csv.shape[0]):
        pocket_fmnsit_te[test_csv_np[i,0]].append(test_csv_np[i,1:] / 255) 
    f_x_train = []
    inx_conters = np.zeros(10, dtype = int)
    for i in index_train_all:
        f_x_train.append(pocket_fmnsit_tr[i][inx_conters[i]])
        inx_conters[i] = (inx_conters[i] + 1) % 6000
    f_x_test = []
    inx_conters = np.zeros(10, dtype = int)
    for i in index_test_all:
        f_x_test.append(pocket_fmnsit_te[i][inx_conters[i]])
        inx_conters[i] = (inx_conters[i] + 1) % 1000
    np.save(db_folder2 + "/data_fi_train.npy", np.array(f_x_train))
    np.save(db_folder2 + "/data_fi_test.npy", np.array(f_x_test))
    np.save(db_folder2 + "/lables_train.npy", np.array(index_train_all))
    np.save(db_folder2 + "/lables_test.npy", np.array(index_test_all))
    #%%
    f_x_train_all = np.load(db_folder2 + "/data_fi_train.npy", allow_pickle=True)
    f_x_test_all = np.load(db_folder2 + "/data_fi_test.npy", allow_pickle=True) 
    
    #%%
    #Prepearing to use the gests DS
    db_folder2 = os.path.dirname(__file__) + "/../Databases/Gests"
    g_x_all = np.load(db_folder2 + "/X.npy", allow_pickle=True)
    g_y_all = np.argmax(np.load(db_folder2 + "/Y.npy", allow_pickle=True), axis = 1)
    
    new_g_x_all = []
    new_size = 28
    objects_dict = {}
    for i in range(10):
        objects_dict[i] = []
    for i in range(len(g_y_all)):
        new_g_x_all.append(cv2.resize(g_x_all[i], (new_size,new_size),))
        objects_dict[g_y_all[i]].append(i)
    g_x_all = np.array(new_g_x_all).reshape((len(g_y_all), new_size*new_size))
    
    new_g_x_all = []
    counters = np.zeros(10, dtype = int)
    for i in index_train:
        new_g_x_all.append(g_x_all[objects_dict[i][counters[i]]])
        counters[i] = (counters[i] + 1) % (3*len(objects_dict[i])//4)
    g_x_train = np.array(new_g_x_all)
    
    new_g_x_all = []
    counters = np.zeros(10, dtype = int)
    for i in index_test:
        new_g_x_all.append(g_x_all[objects_dict[i][(3*len(objects_dict[i])//4) + counters[i]]])
        counters[i] = (counters[i] + 1) % (len(objects_dict[i])//4)
    g_x_test = np.array(new_g_x_all)
    
    
    g_y_train = index_train
    g_x_label = g_x_train[55000:]
    g_y_label = g_y_train[55000:] 
    g_y_test = index_test

    
    #%%
    ######################
    ######## Test the best params########## 
    
    alpha1 = 30.4146800
    alpha2 = 19.7962481
    alpha3 = 71.5760975
    alpha4 = 26.41402191
    lat_importance = 1.68740
    beta1 = 2.6058152695
    beta2 = 5.4773233
    beta3 = 0.72737534
    beta4 = 1.2566059322
    multipl_mode = True
    max_only_mode =  True
    only_bmu = True
    norm_lateral = True
    train_soms_params = ((1, 0.01, 10, 0.01, alpha1),
                     (1, 0.01, 10, 0.01, alpha2),
                     (1, 0.01, 10, 0.01, alpha3),
                     (1, 0.01, 10, 0.01, alpha4))
    
    np.random.seed(77)
    p = np.random.permutation(60000)
    im_x_train = im_x_train[p]
    sp_x_train = sp_x_train[p]
    g_x_train = g_x_train[p]
    f_x_train_all = f_x_train_all[p]
    index_train = index_train[p]
    
    model1 = FE_SOM_Model(load = True, model_filename = "best_mnist_som")
    model2 = FE_SOM_Model(load = True, model_filename = "best_smnist_16som")
    model3 = FE_SOM_Model(load = True, model_filename = "best_gests_som")
    model4 = FE_SOM_Model(load = True, model_filename = "best_fmnist_som")
    resom_model = MMFE_SOM_Model(take_trained_fesoms = True, fesoms = (model1, model2, model3, model4),
                 use_gpu = False, random_lat_weights = False, seed = 777, 
                 labels_list = model1.som.labels_list)
    resom_model.train(X = (im_x_train, sp_x_train, g_x_train, f_x_train_all), Y = index_train,
                      train_lateral = True, train_lat_mod = 'resom',
                      resom_train_params =  (1.,),
                      X_lbls = (im_x_label, sp_x_label, g_x_label,  f_x_train_all[55000:]), Y_lbls = index_label,
                      list_cls = None, labeling = False,
                      train_soms = False, X_test = (im_x_test, sp_x_test, g_x_test, f_x_test_all), Y_test = index_test,
                      train_soms_params = train_soms_params, 
                      num_epochs = 1, verbose = 3, test_each = 1e10,
                      save_activations = False, ignore_activations = False, save_resom_num_evolution = True)     
    acc = resom_model.test((im_x_test, sp_x_test, g_x_test, f_x_test_all), index_test, 
                   metrics = {'acc'}, multiplication_mode = multipl_mode,
                   lat_importance = lat_importance, test_sigma = (beta1, beta2, beta3, beta4),
                   verbose = 1, max_only_mode = max_only_mode, 
                   only_bmu = only_bmu, norm_lateral = norm_lateral)
    print(acc)
    #acc about 0.985
    
    #%%
    ######################
    ######################
    ######## Analise for speed of connection growth ##########
    ######################
    ######################
    import matplotlib.ticker as mtick
    norm_coef = 256*256/100
    num_points_01 = np.array(range(len(resom_model.resom_num_evolution[(0,1)])))/norm_coef
    num_points_02 = np.array(range(len(resom_model.resom_num_evolution[(0,2)])))/norm_coef
    num_points_12 = np.array(range(len(resom_model.resom_num_evolution[(1,2)])))/norm_coef
    num_points_03 = np.array(range(len(resom_model.resom_num_evolution[(0,3)])))/norm_coef
    num_points_13 = np.array(range(len(resom_model.resom_num_evolution[(1,3)])))/norm_coef
    num_points_23 = np.array(range(len(resom_model.resom_num_evolution[(2,3)])))/norm_coef
    plt.plot(resom_model.resom_num_evolution[(0,1)], num_points_01)
    plt.plot(resom_model.resom_num_evolution[(0,2)], num_points_02)
    plt.plot(resom_model.resom_num_evolution[(0,3)], num_points_03)
    plt.plot(resom_model.resom_num_evolution[(1,2)], num_points_12)
    plt.plot(resom_model.resom_num_evolution[(1,3)], num_points_13)
    plt.plot(resom_model.resom_num_evolution[(2,3)], num_points_23)
    ax = plt.gca()
    ax.yaxis.set_major_formatter(mtick.PercentFormatter())
    plt.xlabel("Number of train objects")
    plt.ylabel("part of filled matrix cells, %")
    plt.legend(["0-1","0-2", "0-3","1-2","1-3", "2-3"])
    plt.show()
    
    
      
    #%%
    ######################
    ######################
    ######## Analise connections prunning ##########
    ######################
    ######################
    resom_model = MMFE_SOM_Model(load = True, model_filename = '3_mod_mmresom_msf')
    w = resom_model.resom_lat_dict[(1,2)].connections.numpy().reshape((256*256))
    w = w[w>0]
    pers = 1 - np.logspace(-4, 0, num = 50)[::-1]
    quantiles = np.quantile(w, pers)
    print(quantiles)
    
    alpha1 = 47.32602590092309
    alpha2 = 59.55092825556682
    alpha3 = 4.903210098053965
    lat_importance = 0.9975177843550593
    beta1 = 0.9928867712
    beta2 = 0.127113894
    beta3 = 2.2202536564
    multipl_mode = True
    max_only_mode =  True
    only_bmu = False
    norm_lateral = True
    train_soms_params = ((1, 0.01, 10, 0.01, alpha1),
                     (1, 0.01, 10, 0.01, alpha2),
                     (1, 0.01, 10, 0.01, alpha3))
    accs = []
    
    for k in range(len(quantiles)):
        for i in range(2):
            for j in range(i + 1,3):
                w = resom_model.resom_lat_dict[(i,j)].connections
                w[w<quantiles[k]] = 0
        acc = resom_model.test((im_x_test, sp_x_test, f_x_test_all), index_test, 
               metrics = {'acc'}, test_sigma = (beta1, beta2, beta3),
               verbose = 1, max_only_mode = max_only_mode, 
               only_bmu = only_bmu, norm_lateral = norm_lateral)
        print(acc)
        accs.append(acc)
    print(accs)
    #%%
    import matplotlib.ticker as mtick
    plt.plot((1-pers)[:]*100, accs[:], 'o-')
    plt.axvline(10, ls = '--',)
    ax = plt.gca()
    plt.xscale('log')
    ax.xaxis.set_major_formatter(mtick.PercentFormatter())
    plt.ylabel('Accuracy')
    plt.xlabel('Saved connections, %')
    #plt.yscale('log')
    plt.show()
    
    
    
    
    
    #%%
    ######################
    ######################
    ######## Biuld common image for article  ##########
    ######################
    ######################
    stack_cf_m = [[0,0],[0,0],[0,0],[0,0]]
    #%%
    model0 = FE_SOM_Model(load = True, model_filename = "best_mnist_som")
    acc = model0.test(X = im_x_test, Y = index_test, )
    print(acc)
    conf_matr0 = model0.c_m
    sn.set(font_scale=1) # for label size
    sn.heatmap(conf_matr0, annot=True, annot_kws={"size": 10}, cmap="YlGnBu", vmin=0, vmax=100) # font size
    plt.show()
    stack_cf_m[0][0] = conf_matr0
    #%%
    model1 = FE_SOM_Model(load = True, model_filename = "best_smnist_16som")
    acc = model1.test(X = sp_x_test, Y = index_test, )
    print(acc)
    conf_matr0 = model1.c_m
    sn.set(font_scale=1) # for label size
    sn.heatmap(conf_matr0, annot=True, annot_kws={"size": 10}, cmap="YlGnBu", vmin=0, vmax=100) # font size
    plt.show()
    stack_cf_m[1][0] = conf_matr0
    #%%
    model2 = FE_SOM_Model(load = True, model_filename = "best_gests_som")
    acc = model2.test(X = g_x_test, Y = index_test, )
    print(acc)
    conf_matr0 = model2.c_m
    sn.set(font_scale=1) # for label size
    sn.heatmap(conf_matr0, annot=True, annot_kws={"size": 10}, cmap="YlGnBu", vmin=0, vmax=100) # font size
    plt.show()
    stack_cf_m[2][0] = conf_matr0
    #%%
    model3 = FE_SOM_Model(load = True, model_filename = "best_fmnist_som")
    acc = model3.test(X = f_x_test_all, Y = index_test, )
    print(acc)
    conf_matr0 = model3.c_m
    sn.set(font_scale=1) # for label size
    sn.heatmap(conf_matr0, annot=True, annot_kws={"size": 10}, cmap="YlGnBu", vmin=0, vmax=100) # font size
    plt.show()
    stack_cf_m[3][0] = conf_matr0
    #%%
    alpha1 = 37.82265671
    alpha2 = 16.337380
    lat_importance = 0.0189961441
    beta1 = 1.802932484
    beta2 = 0.2078495214
    multipl_mode = False
    max_only_mode =  True
    only_bmu = False
    norm_lateral = False
    
    train_soms_params = ((1, 0.01, 10, 0.01, alpha1),
                         (1, 0.01, 10, 0.01, alpha2))
    
    model1 = FE_SOM_Model(load = True, model_filename = "best_mnist_som")
    model2 = FE_SOM_Model(load = True, model_filename = "best_smnist_16som")
    resom_model = MMFE_SOM_Model(take_trained_fesoms = True, fesoms = (model1, model2),
                 use_gpu = False, random_lat_weights = False, seed = 777, 
                 labels_list = model1.som.labels_list)
    resom_model.train(X = (im_x_train, sp_x_train), Y = index_train,
                      train_lateral = True, train_lat_mod = 'resom',
                      resom_train_params =  (1.,),
                      X_lbls = (im_x_label, sp_x_label), Y_lbls = index_label,
                      list_cls = None, labeling = False,
                      train_soms = False, X_test = (im_x_test, sp_x_test), Y_test = index_test,
                      train_soms_params = train_soms_params, 
                      num_epochs = 1, verbose = 3, test_each = 1e10,
                      save_activations = False, ignore_activations = False)  
    metric = resom_model.test((im_x_test, sp_x_test), index_test, 
                   metrics = {'acc',  'conf_matr'}, multiplication_mode = multipl_mode,
                   lat_importance = lat_importance, test_sigma = (beta1, beta2),
                   verbose = 1, max_only_mode = max_only_mode, 
                   only_bmu = only_bmu, norm_lateral = norm_lateral)
    print(metric[0])
    labels_numbers = np.unique(index_test, return_counts = True)[1]
    conf_matr2x = (metric[1] /  labels_numbers).T
    sn.set(font_scale=1) # for label size
    sn.heatmap(conf_matr2x*100, annot=True, annot_kws={"size": 10},
               fmt='3.1f',  cmap="YlGnBu", vmin=0, vmax=100) # font size
    plt.show()
    stack_cf_m[0][1] = conf_matr2x*100
    #%%
    alpha1 = 47.32602590092309
    alpha2 = 59.55092825556682
    alpha3 = 4.903210098053965
    lat_importance = 0.9975177843550593
    beta1 = 0.9928867712
    beta2 = 0.127113894
    beta3 = 2.2202536564
    multipl_mode = True
    max_only_mode =  True
    only_bmu = False
    norm_lateral = True
    train_soms_params = ((1, 0.01, 10, 0.01, alpha1),
                     (1, 0.01, 10, 0.01, alpha2),
                     (1, 0.01, 10, 0.01, alpha3))
    
    model1 = FE_SOM_Model(load = True, model_filename = "best_mnist_som")
    model2 = FE_SOM_Model(load = True, model_filename = "best_smnist_16som")
    model3 = FE_SOM_Model(load = True, model_filename = "best_fmnist_som")
    resom_model = MMFE_SOM_Model(take_trained_fesoms = True, fesoms = (model1, model2, model3),
                 use_gpu = False, random_lat_weights = False, seed = 778, 
                 labels_list = model1.som.labels_list)
    resom_model.train(X = (im_x_train, sp_x_train, f_x_train_all), Y = index_train,
                      train_lateral = True, train_lat_mod = 'resom',
                      resom_train_params =  (1.,),
                      X_lbls = (im_x_label, sp_x_label, f_x_train_all[55000:]), Y_lbls = index_label,
                      list_cls = None, labeling = False,
                      train_soms = False, X_test = (im_x_test, sp_x_test, f_x_test_all), Y_test = index_test,
                      train_soms_params = train_soms_params, 
                      num_epochs = 1, verbose = 3, test_each = 1e10,
                      save_activations = False, ignore_activations = False)      
    metric = resom_model.test((im_x_test, sp_x_test, f_x_test_all), index_test, 
                   metrics = {'acc',  'conf_matr'}, multiplication_mode = multipl_mode,
                   lat_importance = lat_importance, test_sigma = (beta1, beta2, beta3),
                   verbose = 1, max_only_mode = max_only_mode, 
                   only_bmu = only_bmu, norm_lateral = norm_lateral)
    print(metric[0])
    labels_numbers = np.unique(index_test, return_counts = True)[1]
    conf_matr3x1 = (metric[1] /  labels_numbers).T
    sn.set(font_scale=1) # for label size
    sn.heatmap(conf_matr3x1*100, annot=True, annot_kws={"size": 9}, cmap="YlGnBu",
               fmt='3.1f', vmin=0, vmax=100) # font size
    plt.show()
    stack_cf_m[1][1] = conf_matr3x1*100
    #%%
    alpha1 = 12.7932229
    alpha2 = 25.532320348
    alpha3 = 1.9221129857
    lat_importance = 1.4055248017
    beta1 = 1.50454864
    beta2 = 0.07741126
    beta3 = 0.51128851117
    multipl_mode = True
    max_only_mode =  True
    only_bmu = True
    norm_lateral = False
    train_soms_params = ((1, 0.01, 10, 0.01, alpha1),
                     (1, 0.01, 10, 0.01, alpha2),
                     (1, 0.01, 10, 0.01, alpha3))
    
    model1 = FE_SOM_Model(load = True, model_filename = "best_mnist_som")
    model2 = FE_SOM_Model(load = True, model_filename = "best_smnist_16som")
    model3 = FE_SOM_Model(load = True, model_filename = "best_gests_som")
    resom_model = MMFE_SOM_Model(take_trained_fesoms = True, fesoms = (model1, model2, model3),
                 use_gpu = False, random_lat_weights = False, seed = 778, 
                 labels_list = model1.som.labels_list)
    resom_model.train(X = (im_x_train, sp_x_train, g_x_train), Y = index_train,
                      train_lateral = True, train_lat_mod = 'resom',
                      resom_train_params =  (1.,),
                      X_lbls = (im_x_label, sp_x_label, g_x_label), Y_lbls = index_label,
                      list_cls = None, labeling = False,
                      train_soms = False, X_test = (im_x_test, sp_x_test, g_x_test), Y_test = index_test,
                      train_soms_params = train_soms_params, 
                      num_epochs = 1, verbose = 3, test_each = 1e10,
                      save_activations = False, ignore_activations = False)     
    metric = resom_model.test((im_x_test, sp_x_test, g_x_test), index_test, 
                   metrics = {'acc',  'conf_matr'}, multiplication_mode = multipl_mode,
                   lat_importance = lat_importance, test_sigma = (beta1, beta2, beta3),
                   verbose = 1, max_only_mode = max_only_mode, 
                   only_bmu = only_bmu, norm_lateral = norm_lateral)
    print(metric[0])
    labels_numbers = np.unique(index_test, return_counts = True)[1]
    conf_matr3x2 = (metric[1] /  labels_numbers).T
    sn.set(font_scale=1) # for label size
    sn.heatmap(conf_matr3x2*100, annot=True, annot_kws={"size": 9}, cmap="YlGnBu",
               fmt='3.1f', vmin=0, vmax=100) # font size
    plt.show()
    stack_cf_m[2][1] = conf_matr3x2*100
    #%%
    #Save learned 4 modalities
    alpha1 = 30.4146800
    alpha2 = 19.7962481
    alpha3 = 71.5760975
    alpha4 = 26.41402191
    lat_importance = 1.68740
    beta1 = 2.6058152695
    beta2 = 5.4773233
    beta3 = 0.72737534
    beta4 = 1.2566059322
    multipl_mode = True
    max_only_mode =  True
    only_bmu = True
    norm_lateral = True
    train_soms_params = ((1, 0.01, 10, 0.01, alpha1),
                     (1, 0.01, 10, 0.01, alpha2),
                     (1, 0.01, 10, 0.01, alpha3),
                     (1, 0.01, 10, 0.01, alpha4))

    model1 = FE_SOM_Model(load = True, model_filename = "best_mnist_som")
    model2 = FE_SOM_Model(load = True, model_filename = "best_smnist_16som")
    model3 = FE_SOM_Model(load = True, model_filename = "best_gests_som")
    model4 = FE_SOM_Model(load = True, model_filename = "best_fmnist_som")
    resom_model = MMFE_SOM_Model(take_trained_fesoms = True, fesoms = (model1, model2, model3, model4),
                 use_gpu = False, random_lat_weights = False, seed = 777, 
                 labels_list = model1.som.labels_list)
    resom_model.train(X = (im_x_train, sp_x_train, g_x_train, f_x_train_all), Y = index_train,
                      train_lateral = True, train_lat_mod = 'resom',
                      resom_train_params =  (1.,),
                      X_lbls = (im_x_label, sp_x_label, g_x_label,  f_x_train_all[55000:]), Y_lbls = index_label,
                      list_cls = None, labeling = False,
                      train_soms = False, X_test = (im_x_test, sp_x_test, g_x_test, f_x_test_all), Y_test = index_test,
                      train_soms_params = train_soms_params, 
                      num_epochs = 1, verbose = 3, test_each = 1e10,
                      save_activations = False, ignore_activations = False, save_resom_num_evolution = True)     
    metric = resom_model.test((im_x_test, sp_x_test, g_x_test, f_x_test_all), index_test, 
                   metrics = {'acc',  'conf_matr'}, multiplication_mode = multipl_mode,
                   lat_importance = lat_importance, test_sigma = (beta1, beta2, beta3, beta4),
                   verbose = 1, max_only_mode = max_only_mode, 
                   only_bmu = only_bmu, norm_lateral = norm_lateral)
    print(metric[0])
    labels_numbers = np.unique(index_test, return_counts = True)[1]
    conf_matr4x = (metric[1] /  labels_numbers).T
    sn.set(font_scale=1) # for label size
    sn.heatmap(conf_matr4x*100, annot=True, annot_kws={"size": 9}, cmap="YlGnBu",
               fmt='3.1f', vmin=0, vmax=100) # font size
    plt.show()
    stack_cf_m[3][1] = conf_matr4x*100
    #%%
    stack_titles = [['a - MNIST SOM','e - MNIST+SMNIST ReSOM'],
                    ['b - SMNIST SOM','f - MNIST+SMNIST+FMNIST ReSOM'],
                    ['c - Gests SOM','g - MNIST+SMNIST+Gests ReSOM'],
                    ['d - FMNIST SOM','h - MNIST+SMNIST+FMNIST+Gests ReSOM']]

    fig, ax = plt.subplots(4, 2, figsize=(12,24))
    for j in range(2):
        for i in range(4):    
            sn.set(font_scale=1) # for label size
            sn.heatmap(stack_cf_m[i][j], annot=True, annot_kws={"size": 9}, cmap="YlGnBu",
                       fmt='3.1f', vmin=0, vmax=100, ax=ax[i,j])
            ax[i,j].set_title(stack_titles[i][j])
    plt.show()
            